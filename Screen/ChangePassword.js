import React, { useState } from "react";
import { I18nManager } from "react-native";
import { Card } from "react-native-elements";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Auth } from "../Service/ApiCall";

import { PrimaryButton } from "office-ui-fabric-react";

import { View, ScrollView } from "react-native";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import AutoSubmitForm from "./AutoSubmitForm";
import "../login.css";
import img_avatar2 from "../assets/img_avatar2.png"; // Tell webpack this JS file uses this image

export default function ChangePasswordScreen(props) {
  const [submit, setSubmit] = useState(false);
  const [userName, setUserName] = useState(null);
  const [newPassword, setNewPassword] = useState(null);
  const [currentPassword, setCurrentPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);

  I18nManager.forceRTL(true);
  const stackTokens = { childrenGap: 10 };

  let _changePassword = async e => {
    try {
      e.preventDefault();

      if (newPassword != confirmPassword) {
        alert("رمز عبور جدید با تکرار آن یکسان نیست");
        return;
      }

      let obj = {
        userName: props.route.params.username,
        currentPassword: currentPassword,
        newPassword: newPassword
      };
      let mo = await Auth.ChangePassword(obj);

      alert("رمز عبور با موفقیت تغییر کرد.");
      props.navigation.push("LoginScreen"); //
    } catch (error) {
      console.log(error);
      alert("در وارد کردن رمز عبور دقت نمایید.");
    }
  };

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Card title="تغییر رمز عبور">
        <form style={{ padding: 25 }} method="post" onSubmit={_changePassword}>
          <div className="ms-Grid" dir="rtl">
            <div className="ms-Grid-row">
              <div className="ms-Grid-col ms-sm6 ms-md12 ms-lg12">
                <div className="imgcontainer">
                  <img src={img_avatar2} alt="Avatar" className="avatar" />
                </div>
              </div>
              <div className="ms-Grid-col ms-sm12 ms-md8 ms-lg12">
                <span>
                  {" "}
                  رمز عبور باید شامل حروف بزرگ و کوچیک و کارکترهای خاص و حداقل 6
                  کاراکتر باشد برای مثال
                </span>
                <span style={{ color: "red" }}>*aA123456</span>
              </div>
            </div>
            <div className="ms-Grid-col ms-sm12 ms-md8 ms-lg12">
              <Stack tokens={stackTokens}>
                <div>
                  <TextField
                    name="currentPassword"
                    type="password"
                    value={currentPassword}
                    label="رمز عبور فعلی :"
                    required
                    onChange={(ev, text) => setCurrentPassword(text)}
                  />
                </div>

                <div>
                  <TextField
                    name="newPassword"
                    type="password"
                    value={newPassword}
                    label="رمز عبور جدید :"
                    required
                    onChange={(ev, text) => setNewPassword(text)}
                  />
                </div>
                <div>
                  <TextField
                    name="confirmPassword"
                    type="password"
                    value={confirmPassword}
                    label="تکرار رمز عبور جدید"
                    required
                    onChange={(ev, text) => setConfirmPassword(text)}
                  />
                </div>
                <div>
                  <PrimaryButton text="ذخیره" type="submit" />
                </div>
              </Stack>
            </div>
          </div>
        </form>
      </Card>
    </View>
  );
}
