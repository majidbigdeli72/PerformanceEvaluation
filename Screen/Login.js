import React, { useState } from "react";
import { I18nManager } from "react-native";
import { Card } from "react-native-elements";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Auth } from "../Service/ApiCall";

import { PrimaryButton } from "office-ui-fabric-react";

import { View, ScrollView } from "react-native";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import AutoSubmitForm from "./AutoSubmitForm";
import "../login.css";
import img_avatar2 from "../assets/img_avatar2.png"; // Tell webpack this JS file uses this image

export default function LoginScreen(props) {
  I18nManager.forceRTL(true);
  const stackTokens = { childrenGap: 10 };

  const [submit, setSubmit] = useState(false);
  const [returnUrl, setReturnUrl] = useState("/LoginViaRequest.aspx");
  const [userName, setUserName] = useState(null);
  const [password, setPassword] = useState(null);
  const [token, setToken] = useState("");

  const _login = async e => {
    e.preventDefault();

    try {
      let obj = {
        userName: userName,
        password: password
      };

      let login = await Auth.Login(obj);

      if (login.data.result == 1) {
        var bodyFormData = new FormData();
        bodyFormData.set("grant_type", "password");
        bodyFormData.set("username", userName);
        bodyFormData.set("password", password);
        bodyFormData.set("client_id", "PerformanceEvaluation_App");
        bodyFormData.set("client_secret", "1q2w3e*");

        let mo = await Auth.Connect(bodyFormData);
        localStorage.setItem("token", mo.data.access_token);
        localStorage.removeItem("NAVIGATION_STATE");
        let url = returnUrl + window.location.search;
        setReturnUrl(url);
        setToken(mo.data.access_token);
        setSubmit(true);
      } else if (login.data.result == 6) {
        props.navigation.push("ChangePasswordScreen", { username: userName }); //
      } else if (login.data.result == 2) {
        alert("نام کاربری یا پسورد اشتباه می باشد.");
      } else {
        alert("مشکل دسترسی دارید لطفا با ادمین صحبت کنید.");
      }
    } catch (error) {
      console.log(error);

      alert("نام کاربری یا پسورد اشتباه است");
    }
  };

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Card title="ورود">
        <form style={{ padding: 25 }} method="post" onSubmit={_login}>
          <div className="imgcontainer">
            <img src={img_avatar2} alt="Avatar" className="avatar" />
          </div>
          <Stack tokens={stackTokens}>
            <div>
              <TextField
                name="UserName"
                value={userName}
                label="نام کاربری :"
                required
                onChange={(ev, text) => setUserName(text)}
              />
            </div>
            <div>
              <TextField
                name="Password"
                type="password"
                value={password}
                label="رمز عبور :"
                required
                onChange={(ev, text) => setPassword(text)}
              />
            </div>
            <div>
              <PrimaryButton text="ورود" type="submit" />
            </div>
          </Stack>
        </form>
        <AutoSubmitForm
          actionUrl={returnUrl}
          params={{ token: token }}
          autoSubmit={submit}
        />
      </Card>
    </View>
  );
}
