import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Card } from "react-native-elements";
import { PALog } from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import moment from "moment-jalaali";

export default function PALogScreen(props) {
  const _columns = [
    {
      key: "column1",
      name: "پیام",
      fieldName: "exception",
      minWidth: 300,
      maxWidth: 650,
      isResizable: true,
      isPadded: true
    },
    {
      key: "column2",
      name: "نام دوره ی ارزیابی",
      fieldName: "paName",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column3",
      name: "نام شاخص",
      fieldName: "piName",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column4",
      name: "سمت / دپارتمان",
      fieldName: "organizationEntryName",
      minWidth: 50,
      maxWidth: 150,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column5",
      name: "نوع",
      fieldName: "paErrorTypeIndex",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      isPadded: true
    },
    {
      key: "column6",
      name: "تاریخ ایجاد",
      fieldName: "relatedDate",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [_allItems, setAllItems] = useState([]);
  const _getPALog = async () => {
    try {
      let mo = await PALog.GetPALogs();
      setAllItems(mo.data.items);
      setState({ items: mo.data.items });
    } catch (error) {}
  };

  useFocusEffect(
    React.useCallback(() => {
      _getPALog();
    }, [])
  );

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "paErrorTypeIndex") {
      if (item.paErrorTypeIndex == 1) {
        return <span style={{ color: "#ff7306", fontWeight: 700 }}>هشدار</span>;
      } else {
        return <span style={{ color: "red", fontWeight: 700 }}>خطا</span>;
      }
    }
    if (column.fieldName === "relatedDate") {
      return moment(item.startDate).format("YYYY-MM-DD");
    }

    return item[column.fieldName];
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <Card title="لیست دوره ی ارزیابی">
          <Fabric>
            <View
              style={{
                height: 500,
                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
