import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Card, Button } from "react-native-elements";
import { PI } from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import { json2html } from "html2json";


export default function PerformanceIndicatorListScreen(props) {
  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "name",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "توضیحات",
      fieldName: "description",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column3",
      name: "عملیات",
      fieldName: "action",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [_allItems, setAllItems] = useState([]);

  const [pI, setPI] = useState(0);

  const _getPI = async () => {
    try {
      let mo = await PI.GetPIs();
      setAllItems(mo.data.items);
      setState({ items: mo.data.items });
    } catch (error) {
      console.log(error);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      _getPI();
    }, [])
  );

  const _onFilter = (ev, text) => {
    var mo = text
      ? _allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1)
      : _allItems;
    setState({
      items: mo
    });
  };

  const _goToEditPI = item => {
    props.navigation.push("CreatePerformanceIndicatorScreen", item);
  };

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link onClick={() => _goToEditPI(item)}>ویرایش</Link>
        </>
      );
    }
    if (column.fieldName === "description") {
      if (item.description) {
        return <div dangerouslySetInnerHTML={{ __html: json2html(JSON.parse(item.description)) }} />
      }
      return <></>
    }

    return item[column.fieldName];
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>

        <Card title="لیست دوره ی ارزیابی">
          <TextField
            required
            underlined
            label="جستجو با عنوان:"
            onChange={_onFilter}
            styles={{ root: { width: 500, marginBottom: 20 } }}
          />
          <Fabric>
            <View
              style={{
                height: 500,

                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );

}




