import React, { useEffect, useRef } from "react";
import UseMountEffect from "./UseMountEffect";
export default function AutoSubmitForm({ actionUrl, params, autoSubmit }) {
  
  if (autoSubmit) {
   return <UseMountEffect actionUrl={actionUrl} params={params} />;
  } else {
    return <></>;
  }
}


