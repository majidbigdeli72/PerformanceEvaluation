import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  ConstrainMode,
  DetailsListLayoutMode,
  Selection
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Card } from "react-native-elements";
import { useFocusEffect } from "@react-navigation/native";
import { Link, ResponsiveMode } from "office-ui-fabric-react";
import { getId } from "office-ui-fabric-react/lib/Utilities";
import { FontSizes } from "@uifabric/styling";
import { ChoiceGroup } from "office-ui-fabric-react/lib/ChoiceGroup";
import { PrimaryButton } from "office-ui-fabric-react";
import {
  QuestionnaireResult,
  QuestionResult,
  Questionnaire,
  PAResultPIAssessor,
  Employee,
  User
} from "../Service/ApiCall";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";
import { Sticky, StickyPositionType } from "office-ui-fabric-react/lib/Sticky";
import {
  ScrollablePane,
  ScrollbarVisibility
} from "office-ui-fabric-react/lib/ScrollablePane";
import {
  getTheme,
  mergeStyleSets,
  FontWeights,
  ContextualMenu,
  Modal,
  IconButton
} from "office-ui-fabric-react";
export function PAResultPIAssessors(props) {
  const _selection = new Selection();
  const classNames = mergeStyleSets({
    wrapper: {
      height: "75vh",
      position: "relative"
    },
    filter: {
      paddingBottom: 20,
      maxWidth: 300
    },
    header: {
      margin: 0
    },
    row: {
      display: "inline-block"
    }
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [
    modalVisibleWithoutQuestion,
    setModalVisibleWithoutQuestion
  ] = useState(false);

  const [modalChangeAssessor, setModalChangeAssessor] = useState(false);
  const [modalChangeEmployee, setModalChangeEmployee] = useState(false);
  const [assessorId, setAssessorId] = useState(null);
  const [pAResultPIAssessorId, setPAResultPIAssessorId] = useState(null);
  const [pAResultPIId, setPAResultPIId] = useState(null);
  const [state, setState] = useState({
    items: []
  });
  const [_allItems, setAllItems] = useState([]);
  const [employeeOption, setEmployeeOption] = useState([]);
  const [result, setResult] = useState([]);
  const [resultWithoutQuestion, setResultWithoutQuestion] = useState({});
  const [value, setValue] = useState(null);
  const [eState, setEState] = useState(false);
  const [performanceIndicatorName, setPerformanceIndicatorName] = useState(
    null
  );
  const [employeeSelectedKeys, setEmployeeSelectedKeys] = useState(null);
  const [qustionnaire, setQustionnaire] = useState({
    name: "",
    description: "",
    id: "",
    questions: []
  });
  const theme = getTheme();
  const contentStyles = mergeStyleSets({
    container: {
      display: "flex",
      flexFlow: "column nowrap",
      alignItems: "stretch",
      minWidth: "40%"
    },
    header: [
      // tslint:disable-next-line:deprecation
      theme.fonts.xLargePlus,
      {
        flex: "1 1 auto",
        borderTop: `4px solid ${theme.palette.themePrimary}`,
        color: theme.palette.neutralPrimary,
        display: "flex",
        fontSize: FontSizes.xLarge,
        alignItems: "center",
        fontWeight: FontWeights.semibold,
        padding: "12px 12px 14px 24px"
      }
    ],
    body: {
      flex: "4 4 auto",
      padding: "0 24px 24px 24px",
      overflowY: "hidden",
      selectors: {
        p: {
          margin: "14px 0"
        },
        "p:first-child": {
          marginTop: 0
        },
        "p:last-child": {
          marginBottom: 0
        }
      }
    }
  });
  const iconButtonStyles = mergeStyleSets({
    root: {
      color: theme.palette.neutralPrimary,
      marginLeft: "auto",
      marginTop: "4px",
      marginRight: "2px"
    },
    rootHovered: {
      color: theme.palette.neutralDark
    }
  });
  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "fullName",
      minWidth: 100,
      maxWidth: 100,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "جایگاه",
      fieldName: "membership",
      minWidth: 100,
      maxWidth: 100,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column3",
      name: "شاخص",
      fieldName: "performanceIndicatorName",
      minWidth: 150,
      maxWidth: 250,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column4",
      name: "دوره ی ارزیابی",
      fieldName: "paName",
      minWidth: 200,
      maxWidth: 250,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column5",
      name: "دسته بندی",
      fieldName: "operationEvaluationCategoryName",
      minWidth: 100,
      maxWidth: 100,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column6",
      name: "نحوه ارزیابی",
      fieldName: "evaluationTypeIndex",
      minWidth: 100,
      maxWidth: 100,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column7",
      name: "چرخش",
      fieldName: "iterationNumber",
      minWidth: 50,
      maxWidth: 50,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column8",
      name: "وضعیت",
      fieldName: "assessStatusIndex",
      minWidth: 120,
      maxWidth: 120,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column9",
      name: "عملیات",
      fieldName: "action",
      minWidth: 370,
      maxWidth: 370,
      isResizable: true,
      isPadded: true
    }
  ];
  const _getPaResultPIAssessor = async () => {
    try {
      let mo = await PAResultPIAssessor.GetPAResultPIAssessor();
      setAllItems(mo.data.items);
      setState({ items: mo.data.items });
    } catch (error) {
      console.log(error);
    }
  };
  useFocusEffect(
    React.useCallback(() => {
      _getPaResultPIAssessor();
      _hasPaAdminRole();
    }, [])
  );
  const _hasPaAdminRole = async () => {
    try {
      let data = {
        roleName: "paadmin"
      };
      let mo = await User.HasRole(data);

      if (mo.data) {
        setEState(true);
        _getEmployee();
      }
    } catch (error) {
      alert(error);
    }
  };
  const _onFilter = (ev, text) => {
    var mo = text
      ? _allItems.filter(i => i.fullName.toLowerCase().indexOf(text) > -1)
      : _allItems;
    setState({
      items: mo
    });
  };
  let _titleId = getId("title");
  let _subtitleId = getId("subText");
  let _dragOptions = {
    moveMenuItemText: "Move",
    closeMenuItemText: "Close",
    menu: ContextualMenu
  };
  const _showQuestionnaire = async item => {
    try {
      if (item.evaluationTypeIndex == 0) {
        let mo = await Questionnaire.GetQuestionnaire(item.questionnaireId);
        let moQs = [];
        if (item.questionnaireResultId) {
          let res = await QuestionResult.GetQuestionResult(
            item.questionnaireResultId
          );
          moQs = res.data;
        }
        let q = mo.data;
        let f = {
          id: 0,
          name: "",
          description: "",
          questions: []
        };
        f.description = q.description;
        f.id = q.id;
        f.name = q.name;
        f.questions = [];
        let resu = [];
        q.questions.forEach(qu => {
          let fo = moQs.find(o => o.questionId === qu.id);
          let def = "";
          if (fo) {
            def = fo.selectedQuestionItemId;
            let qit = qu.questionItems.find(o => o.id === def);
            let qitVal = 0;
            if (qit) {
              qitVal = qit.value;
            }
            let objDef = {
              pAResultAssessorId: item.id,
              value: qitVal,
              questionId: fo.questionId,
              selectedItemId: def,
              calculationTypeIndex: item.calculationTypeIndex,
              questionnaireId: item.questionnaireId,
              weight: qu.weight
            };
            resu.push(objDef);
          }
          let obj = {
            name: qu.content,
            id: qu.id,
            weight: qu.weight,
            default: def,
            items: []
          };

          var quQI = qu.questionItems.sort((a, b) =>
            a.value > b.value ? 1 : b.value > a.value ? -1 : 0
          );

          quQI.forEach(qi => {
            let objItem = {
              key: qi.id,
              text: qi.name,
              value: qi.value,
              questionId: qi.questionId,
              pAResultAssessorId: item.id,
              calculationTypeIndex: item.calculationTypeIndex,
              questionnaireId: item.questionnaireId,
              weight: qu.weight
            };

            obj.items.push(objItem);
          });
          f.questions.push(obj);
        });
        setResult(resu);
        setQustionnaire(f);
        setModalVisible(true);
      } else {
        setPerformanceIndicatorName(item.performanceIndicatorName);
        setValue(item.value);

        let obj = {
          calculationTypeIndex: item.calculationTypeIndex,
          pAResultAssessorId: item.id,
          paOrganizationEntryCategoryPIId: item.paOrganizationEntryCategoryPIId
        };
        setResultWithoutQuestion(obj);
        setModalVisibleWithoutQuestion(true);
      }
    } catch (error) {
      console.log(error);
    }
  };
  let _closeModal = () => {
    setResult([]);
    setModalVisible(false);
  };
  let _closeModalWithoutQuestioneries = () => {
    setResultWithoutQuestion({});
    setValue(null);
    setModalVisibleWithoutQuestion(false);
  };
  let _closeModalChangeAssessor = () => {
    setPAResultPIAssessorId(null);
    setAssessorId(null);
    setPAResultPIId(null);
    setModalChangeAssessor(false);
    setModalChangeEmployee(false);
  };
  let setByPass = async item => {
    if (confirm("آیا مطمئن هستید؟")) {
      try {
        let mo = await PAResultPIAssessor.ByPassResultPIAssessor(item.id);
        alert("با موفقیت وضعیت تغییر کرد");
      } catch (error) {
        alert(error);
      }
      _getPaResultPIAssessor();
    }
  };
  let showChangeAssessorModal = item => {
    setPAResultPIAssessorId(item.id);
    setModalChangeAssessor(true);
  };

  let showChangeEmployeeModal = item => {
    setPAResultPIId(item.paResultPIId);
    setModalChangeEmployee(true);
  };

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      let evaluate = (
        <Link
          key="1"
          style={{ marginRight: 10 }}
          onClick={() => _showQuestionnaire(item)}
        >
          ارزیابی کردن
        </Link>
      );
      let byPass = (
        <Link
          key="2"
          style={{ marginRight: 10, color: "red" }}
          onClick={() => setByPass(item)}
        >
          منصرف شدن
        </Link>
      );
      let changeAssessor = (
        <Link
          key="3"
          style={{ marginRight: 10 }}
          onClick={() => showChangeAssessorModal(item)}
        >
          تغییر ارزیابی کننده{" "}
        </Link>
      );
      let changeEmploye = (
        <Link
          key="4"
          style={{ marginRight: 10 }}
          onClick={() => showChangeEmployeeModal(item)}
        >
          تغییر ارزیابی شونده{" "}
        </Link>
      );

      if (eState) {
        if (item.assessStatusIndex == 2) {
          return <></>;
        } else if (item.isEditAble) {
          return [evaluate, byPass, changeAssessor, changeEmploye];
        } else {
          return [byPass, changeAssessor, changeEmploye];
        }
      } else {
        if (item.isEditAble && item.assessStatusIndex != 2) {
          return [evaluate];
        } else {
          return <></>;
        }
      }
    }

    if (column.fieldName === "evaluationTypeIndex") {
      if (item.evaluationTypeIndex == 0) {
        return <span>پرسشنامه</span>;
      } else {
        return <span>دستی</span>;
      }
    }

    if (column.fieldName === "assessStatusIndex") {
      if (item.assessStatusIndex == 0) {
        return <span style={{ color: "red" }}>ارزیابی نشده</span>;
      } else if (item.assessStatusIndex == 1) {
        return <span style={{ color: "green" }}>ارزیابی شده</span>;
      } else {
        return <span>کنسل شده</span>;
      }
    }

    return item[column.fieldName];
  };
  const _getEmployee = async () => {
    let mo = await Employee.GetEmployees();
    let _option = [];
    mo.data.items.forEach(element => {
      _option.push({
        key: element.employee.id,
        text: element.employee.fullName
      });
    });
    setEmployeeOption(_option);
  };
  const _onChange = async (ev, option) => {
    let res = [...result];
    var removeIndex = res
      .map(function (item) {
        return item.questionId;
      })
      .indexOf(option.questionId);
    if (removeIndex > -1) {
      res.splice(removeIndex, 1);
    }

    let obj = {
      pAResultAssessorId: option.pAResultAssessorId,
      value: option.value,
      questionId: option.questionId,
      selectedItemId: option.key,
      calculationTypeIndex: option.calculationTypeIndex,
      questionnaireId: option.questionnaireId,
      weight: option.weight
    };

    res.push(obj);
    setResult(res);
  };
  const _submit = async e => {
    e.preventDefault();
    try {
      let data = result;
      let mo = await QuestionnaireResult.CreateQuestionnaireResult(data);
      alert("با موفقیت ثبت شد.");
    } catch (error) {
      alert("خطایی رخ داده است");
    }
    _getPaResultPIAssessor();
    _closeModal();
  };
  const _submitWithoutQuestion = async e => {
    e.preventDefault();
    let obj = resultWithoutQuestion;
    obj.value = value;
    setResultWithoutQuestion(obj);

    try {
      let mo = await PAResultPIAssessor.savePAResultPIAssessor(obj);
      alert("با موفقیت ثبت شد.");
    } catch (error) {
      alert("خطایی رخ داده است");
      console.log(error);
    }

    _getPaResultPIAssessor();
    _closeModalWithoutQuestioneries();
  };
  const _submitChangeAssessor = async e => {
    e.preventDefault();
    try {
      let obj = {
        paResultPIAssessorId: pAResultPIAssessorId,
        assessedById: assessorId
      };
      let mo = await PAResultPIAssessor.ChangeAssessor(obj);
      alert("با موفقیت انجام شد.");
    } catch (error) {
      alert(error);
    }
    _closeModalChangeAssessor();
    _getPaResultPIAssessor();
  };
  const _submitChangeEmployee = async e => {
    e.preventDefault();
    try {
      let obj = {
        paResultPIId: pAResultPIId,
        employeeId: assessorId
      };
      let mo = await PAResultPIAssessor.ChangeEmployee(obj);
      alert("با موفقیت انجام شد.");
    } catch (error) {
      alert(error);
    }
    _closeModalChangeAssessor();
    _getPaResultPIAssessor();
  };
  const _onEmployeeDropdownChanged = async (h, e, i) => {
    let assessorId = e.key;
    setEmployeeSelectedKeys(assessorId);
    try {
      let mo = await PAResultPIAssessor.GetPAResultPIAssessorById(assessorId);
      setAllItems(mo.data.items);
      setState({ items: mo.data.items });
    } catch (error) {
      console.log(error);
    }
  };
  const _onAssessorDropdownChanged = (h, e, i) => {
    let assessorId = e.key;
    setAssessorId(assessorId);
  };
  const _employee = () => {
    if (eState) {
      return (
        <Dropdown
          label="کارمند"
          placeholder="انتخاب کارمند"
          options={employeeOption}
          styles={{ root: { width: 500, marginBottom: 20 } }}
          selectedKey={employeeSelectedKeys}
          onChange={_onEmployeeDropdownChanged.bind(this)}
        />
      );
    } else {
      return <></>;
    }
  };
  const _onItemInvoked = item => {
    console.log(item);
    if (item.isEditAble && item.assessStatusIndex != 2) {
      _showQuestionnaire(item);
    }
  };
  return (
    <Stack tokens={{ childrenGap: 15 }}>
      <div className="ms-Grid-row">
        <Modal
          responsiveMode={ResponsiveMode.unknown}
          titleAriaId={_titleId}
          subtitleAriaId={_subtitleId}
          isOpen={modalVisible}
          onDismiss={_closeModal}
          isBlocking={true}
          containerClassName={contentStyles.container}
        >
          <div className={contentStyles.header}>
            <span id={_titleId}>{qustionnaire.name}</span>
            <IconButton
              styles={iconButtonStyles}
              iconProps={{ iconName: "Cancel" }}
              ariaLabel="Close popup modal"
              onClick={_closeModal}
            />
          </div>
          <form method="post" onSubmit={_submit}>
            <div className="ms-Grid-col ms-sm12 ms-md12 ms-lg12">
              <div id={_subtitleId} className={contentStyles.body}>
                {qustionnaire.questions.map((item, index) => (
                  <ChoiceGroup
                    key={index}
                    options={item.items}
                    onChange={_onChange}
                    defaultSelectedKey={item.default}
                    label={item.name}
                    required={true}
                  />
                ))}
              </div>
            </div>
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <PrimaryButton text="ارسال" allowDisabledFocus type="submit" />
            </div>
          </form>
        </Modal>
      </div>

      <div className="ms-Grid-row">
        <Modal
          responsiveMode={ResponsiveMode.unknown}
          titleAriaId={_titleId}
          subtitleAriaId={_subtitleId}
          isOpen={modalVisibleWithoutQuestion}
          onDismiss={_closeModalWithoutQuestioneries}
          isBlocking={true}
          containerClassName={contentStyles.container}
          dragOptions={_dragOptions}
        >
          <div className={contentStyles.header}>
            <span style={{ fontSize: 17 }} id={_titleId}>
              {performanceIndicatorName}
            </span>
            <IconButton
              styles={iconButtonStyles}
              iconProps={{ iconName: "Cancel" }}
              ariaLabel="Close popup modal"
              onClick={_closeModalWithoutQuestioneries}
            />
          </div>
          <form method="post" onSubmit={_submitWithoutQuestion}>
            <TextField
              required
              underlined
              label="عدد مورد نظر را وارد نمایید :"
              type="number"
              inputMode="decimal"
              onChange={(ev, text) => setValue(text)}
              value={value}
              styles={{ root: { width: 500, marginBottom: 20 } }}
            />
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <PrimaryButton text="ارسال" allowDisabledFocus type="submit" />
            </div>
          </form>
        </Modal>
      </div>

      <div className="ms-Grid-row">
        <Modal
          responsiveMode={ResponsiveMode.unknown}
          titleAriaId={_titleId}
          subtitleAriaId={_subtitleId}
          isOpen={modalChangeAssessor}
          onDismiss={_closeModalChangeAssessor}
          isBlocking={true}
          containerClassName={contentStyles.container}
        >
          <div className={contentStyles.header}>
            <span style={{ fontSize: 17 }} id={_titleId}>
              تغییر ارزیابی کننده
            </span>
            <IconButton
              styles={iconButtonStyles}
              iconProps={{ iconName: "Cancel" }}
              ariaLabel="Close popup modal"
              onClick={_closeModalChangeAssessor}
            />
          </div>
          <form method="post" onSubmit={_submitChangeAssessor}>
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <Dropdown
                label="کارمند"
                placeholder="انتخاب کارمند"
                options={employeeOption}
                styles={{ root: { width: 500, marginBottom: 20 } }}
                // selectedKey={cHAemployeeSelectedKeys}
                onChange={_onAssessorDropdownChanged.bind(this)}
                required={true}
              />
            </div>
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <PrimaryButton text="ارسال" allowDisabledFocus type="submit" />
            </div>
          </form>
        </Modal>
      </div>

      <div className="ms-Grid-row">
        <Modal
          responsiveMode={ResponsiveMode.unknown}
          titleAriaId={_titleId}
          subtitleAriaId={_subtitleId}
          isOpen={modalChangeEmployee}
          onDismiss={_closeModalChangeAssessor}
          isBlocking={true}
          containerClassName={contentStyles.container}
        >
          <div className={contentStyles.header}>
            <span style={{ fontSize: 17 }} id={_titleId}>
              تغییر ارزیابی شونده
            </span>
            <IconButton
              styles={iconButtonStyles}
              iconProps={{ iconName: "Cancel" }}
              ariaLabel="Close popup modal"
              onClick={_closeModalChangeAssessor}
            />
          </div>
          <form method="post" onSubmit={_submitChangeEmployee}>
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <Dropdown
                label="کارمند"
                placeholder="انتخاب کارمند"
                options={employeeOption}
                styles={{ root: { width: 500, marginBottom: 20 } }}
                // selectedKey={cHAemployeeSelectedKeys}
                onChange={_onAssessorDropdownChanged.bind(this)}
                required={true}
              />
            </div>
            <div
              className="ms-Grid-col ms-sm12 ms-md12 ms-lg12"
              style={{ padding: 15 }}
            >
              <PrimaryButton text="ارسال" allowDisabledFocus type="submit" />
            </div>
          </form>
        </Modal>
      </div>

      <div className="ms-Grid-row">
        <Card title="لیست ارزیابی ها">
          <div className={classNames.wrapper}>
            <ScrollablePane scrollbarVisibility={ScrollbarVisibility.auto}>
              <Sticky stickyPosition={StickyPositionType.Header}>
                <TextField
                  required
                  underlined
                  label="جستجو با عنوان:"
                  onChange={_onFilter}
                  styles={{ root: { width: 500, marginBottom: 20 } }}
                />
              </Sticky>
              <Sticky stickyPosition={StickyPositionType.Header}>
                <_employee></_employee>
              </Sticky>
              <MarqueeSelection selection={_selection}>
                <DetailsList
                  style={{ maxHeight: 500 }}
                  isHeaderVisible={true}
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  selection={_selection}
                  constrainMode={ConstrainMode.unconstrained}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </ScrollablePane>
          </div>
        </Card>
      </div>
    </Stack>
  );
}

export const PAResultPIAssessorScreen = React.memo(PAResultPIAssessors);
