import React, { useEffect, useRef } from "react";
export default function useMountEffect({ actionUrl, params }) {
    const formRef = useRef(null);
    useEffect(() => {
      formRef.current.submit();
    }, []);
  
    return (
      <form ref={formRef} method="POST" action={actionUrl}>
        {Object.keys(params).map(name => (
          <div key={name}>
            <input type="hidden" name={name} value={params[name]}></input>
          </div>
        ))}
      </form>
    );
  }