import React, { useState } from "react";
import { View, I18nManager } from "react-native";
import { Nav } from "office-ui-fabric-react/lib/Nav";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CreateQuestionnaireScreen from "./CreateQuestionnaire";
import CreateEvaluationScreen from "./CreateEvaluation";
import QuestionnaireListScreen from "./QuestionnaireList";
import EvaluationListScreen from "./EvaluationList";
import PAOrganizationEntryScreen from "./PAOrganizationEntryList";
import PAOrganizationEntryCategoryScreen from "./PAOrganizationEntryCategory";
import PAOrganizationEntryCategoryPIScreen from "./PAOrganizationEntryCategoryPI";
import PAOrganizationEntryCategoryPIAssessorScreen from "./PAOrganizationEntryCategoryPIAssessor";
import CreatePerformanceIndicatorScreen from "./CreatePerformanceIndicator";
import { PAResultPIAssessorScreen } from "./PAResultPIAssessor";
import PerformanceIndicatorListScreen from "./PerformanceIndicatorList";

import PALogScreen from "./ReportPALog";

import "../app.css";
import { initializeIcons } from "@uifabric/icons";
initializeIcons();

const Stack = createStackNavigator();

export default function Dashboard(props) {
  I18nManager.forceRTL(true);

  const _onLinkClick = (ev, item) => {
    props.navigation.navigate(item.key);
  };
  return (
    <View>
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ flex: 1 }}>
          <Nav
            onLinkClick={_onLinkClick}
            styles={{
              root: {
                flex: 1,
                height: "100vh",
                boxSizing: "border-box",
                border: "1px solid #eee",
                overflowY: "auto",
                background: "#fff"
              }
            }}
            ariaLabel="Nav example similiar to one found in this demo page"
            groups={[
              {
                name: "کارتابل",
                links: [
                  // {
                  //   key: "Profile",
                  //   name: "پروفایل"
                  // },
                  {
                    key: "PAResultPIAssessorScreen",
                    name: "ارزیابی ها"
                  }
                ]
              },
              {
                name: "پرسشنامه",
                links: [
                  {
                    key: "QuestionnaireListScreen",
                    name: "لیست پرسشنامه ها"
                  },
                  {
                    key: "CreateQuestionnaireScreen",
                    name: "ایجاد پرسشنامه"
                  }
                ]
              },

              {
                name: "دوره ی ارزیابی",
                links: [
                  {
                    key: "EvaluationListScreen",
                    name: "لیست دوره ی ارزیابی ها"
                  },
                  {
                    key: "CreateEvaluationScreen",
                    name: "ایجاد دوره ی ارزیابی"
                  }
                ]
              },

              {
                name: "شاخص عملکرد",
                links: [
                  {
                    key: "PerformanceIndicatorListScreen",
                    name: "لیست شاخص های عملکرد"
                  },
                  {
                    key: "CreatePerformanceIndicatorScreen",
                    name: "ایجاد شاخص عملکرد"
                  }
                ]
              },

              {
                name: "لاگ ها",
                links: [
                  {
                    key: "PALogScreen",
                    name: "مشکلات دوره های ارزیابی"
                  }
                ]
              }
            ]}
          />
        </View>
        <View style={{ flex: 5 }}>
          <Stack.Navigator initialRouteName={"PAResultPIAssessorScreen"}>
            <Stack.Screen
              options={{ title: "ایجاد پرسشنامه" }}
              name="CreateQuestionnaireScreen"
              key="CreateQuestionnaireScreen"
              component={CreateQuestionnaireScreen}
            />
            <Stack.Screen
              options={{ title: "لیست پرسشنامه" }}
              name="QuestionnaireListScreen"
              key="QuestionnaireListScreen"
              component={QuestionnaireListScreen}
            />
            <Stack.Screen
              options={{ title: "ایجاد دوره ی ارزیابی" }}
              name="CreateEvaluationScreen"
              key="CreateEvaluationScreen"
              component={CreateEvaluationScreen}
            />
            <Stack.Screen
              options={{ title: "لیست دوره ی ارزیابی" }}
              name="EvaluationListScreen"
              key="EvaluationListScreen"
              component={EvaluationListScreen}
            />
            <Stack.Screen
              options={{ title: "سمت های دوره ی ارزیابی" }}
              name="PAOrganizationEntryScreen"
              key="PAOrganizationEntryScreen"
              component={PAOrganizationEntryScreen}
            />
            <Stack.Screen
              options={{ title: "دسته بندی های سمت در دوره ی ارزیابی" }}
              name="PAOrganizationEntryCategoryScreen"
              key="PAOrganizationEntryCategoryScreen"
              component={PAOrganizationEntryCategoryScreen}
            />
            <Stack.Screen
              options={{ title: "ارزیابی های دسته بندی سمت" }}
              name="PAOrganizationEntryCategoryPIScreen"
              key="PAOrganizationEntryCategoryPIScreen"
              component={PAOrganizationEntryCategoryPIScreen}
            />
            <Stack.Screen
              options={{ title: "ارزیابی کننده ها" }}
              name="PAOrganizationEntryCategoryPIAssessorScreen"
              key="PAOrganizationEntryCategoryPIAssessorScreen"
              component={PAOrganizationEntryCategoryPIAssessorScreen}
            />
            <Stack.Screen
              options={{ title: "ارزیابی ها" }}
              name="PAResultPIAssessorScreen"
              key="PAResultPIAssessorScreen"
              component={PAResultPIAssessorScreen}
            />
            <Stack.Screen
              options={{ title: "مشکلات دوره های ارزیابی" }}
              name="PALogScreen"
              key="PALogScreen"
              component={PALogScreen}
            />
            <Stack.Screen
              options={{ title: "ایجاد شاخص" }}
              name="CreatePerformanceIndicatorScreen"
              key="CreatePerformanceIndicatorScreen"
              component={CreatePerformanceIndicatorScreen}
            />
            <Stack.Screen
              options={{ title: "لیست شاخص های عملکرد" }}
              name="PerformanceIndicatorListScreen"
              key="PerformanceIndicatorListScreen"
              component={PerformanceIndicatorListScreen}
            />
          </Stack.Navigator>
        </View>
      </View>
    </View>
  );
}
