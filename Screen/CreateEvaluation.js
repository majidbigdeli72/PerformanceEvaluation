import React, { useState } from "react";
import { View, ScrollView } from "react-native";
import { Button, Card } from "react-native-elements";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import DatePicker from "../Component/Calender/index";
import { PA } from "../Service/ApiCall";
import moment from "moment-jalaali";
import { useFocusEffect } from "@react-navigation/native";

export default function CreateEvaluationScreen(props) {
  const [name, setName] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [endDateMoment, setEndDateMoment] = useState(null);
  const [startDateMoment, setStartDateMoment] = useState(null);
  const [pAId, setPAId] = useState(null);
  const [daysBeforeDueDateWeekly, setDaysBeforeDueDateWeekly] = useState(0);
  const [daysBeforeDueDateMonthly, setDaysBeforeDueDateMonthly] = useState(0);
  const [daysBeforeDueDateQuarterly, setDaysBeforeDueDateQuarterly] = useState(
    0
  );
  const [daysBeforeDueDateHalfly, setDaysBeforeDueDateHalfly] = useState(0);
  const [daysBeforeDueDateYearly, setDaysBeforeDueDateYearly] = useState(0);

  const _getPA = async id => {
    let mo = await PA.GetPA(id);
    setName(mo.data.name);
    setPAId(mo.data.id);
    setStartDate(moment(mo.data.startDate, "jYYYY-jMM-jDDTHH:mm:ss").format());
    setEndDate(moment(mo.data.endDate, "jYYYY-jMM-jDDTHH:mm:ss").format());
    setStartDateMoment(moment(mo.data.startDate, "jYYYY-jMM-jDDTHH:mm:ss"));
    setEndDateMoment(moment(mo.data.endDate, "jYYYY-jMM-jDDTHH:mm:ss"));
    setDaysBeforeDueDateWeekly(mo.data.daysBeforeDueDateWeekly);
    setDaysBeforeDueDateMonthly(mo.data.daysBeforeDueDateMonthly);
    setDaysBeforeDueDateQuarterly(mo.data.daysBeforeDueDateQuarterly);
    setDaysBeforeDueDateHalfly(mo.data.daysBeforeDueDateHalfly);
    setDaysBeforeDueDateYearly(mo.data.daysBeforeDueDateYearly);
  };

  useFocusEffect(
    React.useCallback(() => {
      if (props.route.params) {
        _getPA(props.route.params.id);
      }
    }, [])
  );

  const SavePA = async () => {
    let model = {
      name: name,
      startDate: startDate,
      endDate: endDate,
      daysBeforeDueDateWeekly: daysBeforeDueDateWeekly,
      daysBeforeDueDateMonthly: daysBeforeDueDateMonthly,
      daysBeforeDueDateQuarterly: daysBeforeDueDateQuarterly,
      daysBeforeDueDateHalfly: daysBeforeDueDateHalfly,
      daysBeforeDueDateYearly: daysBeforeDueDateYearly
    };

    if (pAId > 0) {
      var mo = await PA.UpdatePA(pAId, model);
      if (mo.data.id > 0) {
        alert("باموفقیت ویرایش شد");
        ResetState();
      }
    } else {
      var mo = await PA.CreatePA(model);
      if (mo.data.id > 0) {
        alert("باموفقیت ثبت شد");
        ResetState();
      }
    }
  };

  const ResetState = () => {
    setName("");
    //  setStartDate(null);
    //  setEndDate(null);
  };

  return (
    <ScrollView>
      <Stack tokens={{ childrenGap: 15 }}>
        <Card title="ایجاد دوره ی ارزیابی">
          <View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="نام دوره ی ارزیابی:"
                required
                value={name}
                underlined
                onChange={(ev, name) => setName(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <DatePicker
                title="تاریخ شروع ارزیابی"
                required={true}
                isGregorian={false}
                timePicker={false}
                inputReadOnly={true}
                value={startDateMoment}
                onChange={value => {
                  setStartDate(value.format());
                }}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <DatePicker
                title="تاریخ پایان ارزیابی"
                required={true}
                isGregorian={false}
                timePicker={false}
                inputReadOnly={true}
                value={endDateMoment}
                onChange={value => {
                  setEndDate(value.format());
                }}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="چند روز قبل از پایان هفته:"
                required
                type="number"
                value={daysBeforeDueDateWeekly}
                underlined
                onChange={(ev, name) => setDaysBeforeDueDateWeekly(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="چند روز قبل از پایان ماه:"
                required
                type="number"
                value={daysBeforeDueDateMonthly}
                underlined
                onChange={(ev, name) => setDaysBeforeDueDateMonthly(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="چند روز قبل از پایان فصل:"
                required
                type="number"
                value={daysBeforeDueDateQuarterly}
                underlined
                onChange={(ev, name) => setDaysBeforeDueDateQuarterly(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="چند روز قبل از پایان 6 ماه:"
                required
                value={daysBeforeDueDateHalfly}
                underlined
                onChange={(ev, name) => setDaysBeforeDueDateHalfly(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <TextField
                label="چند روز قبل از پایان سال:"
                required
                type="number"
                value={daysBeforeDueDateYearly}
                underlined
                onChange={(ev, name) => setDaysBeforeDueDateYearly(name)}
              />
            </View>
            <View style={{ width: "40%", margin: 20 }}>
              <Button
                icon={{
                  name: "save",
                  size: 20,
                  color: "white"
                }}
                title="ذخیره"
                titleStyle={{ fontSize: 15 }}
                onPress={SavePA}
                buttonStyle={{
                  borderRadius: 25,
                  width: 150,
                  backgroundColor: "green",
                  marginTop: 20
                }}
              />
            </View>
          </View>
        </Card>
      </Stack>
    </ScrollView>
  );
}
