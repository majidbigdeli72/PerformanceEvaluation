import React, { useState } from "react";
import { View, ScrollView } from "react-native";
import { Button, Card } from "react-native-elements";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { PI } from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { html2json, json2html } from "html2json";
import { Label } from 'office-ui-fabric-react/lib/Label';
import CKEditor from "react-ckeditor-component";

export default function CreatePerformanceIndicatorScreen(props) {
    const [name, setName] = useState("");
    const [description, setDescription] = useState(undefined);

    const _getPI = async id => {
        let mo = await PI.GetPI(id);
        setName(mo.data.name);
        if (mo.data.description) {
            setDescription(`${json2html(JSON.parse(mo.data.description))}`);
        }else{
            setDescription("");
        }
    };

    useFocusEffect(
        React.useCallback(() => {
            if (props.route.params) {
                _getPI(props.route.params.id);
            }else{
                setDescription("");
            }
        }, [])
    );


    const save = async () => {
        var obj = {};
        obj.name = name;
        if (description) {
            obj.description = JSON.stringify(html2json(description));
        }
        try {
            if (props.route.params) {
                await PI.UpdatePI(props.route.params.id, obj);
            } else {
                await PI.CreatePI(obj);
            }
            alert("با موفقیت ذخیره شد.")
            setDescription("");
            setName("");
        } catch (error) {
            console.log(error);
        }
    }

    const onChange = (event) => {
        const data = event.editor.getData();
        setDescription(data);

    }


    if (typeof description === 'undefined') {
        console.log("dddddddrrrrrrrrrrrrrrrrrrrrrrrr")
        return <></>
    }

    return (
        <ScrollView>
            <Stack tokens={{ childrenGap: 15 }}>
                <Card title="ایجاد شاخص">
                    <View style={{ width: "40%", margin: 20 }}>
                        <TextField
                            label="نام شاخص :"
                            required
                            value={name}
                            underlined
                            onChange={(ev, name) => setName(name)}
                        />
                    </View>
                    <View style={{ width: "80%", margin: 20 }}>
                        <Label>توضیحات :</Label>
                        <CKEditor
                            content={description}
                            scriptUrl="https://cdn.ckeditor.com/4.14.0/full-all/ckeditor.js"
                            config={{
                                language: "fa",
                                toolbarGroups : [
                                    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                                    { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                                    { name: 'links' },
                                    { name: 'insert' },
                                    { name: 'forms' },
                                    { name: 'tools' },
                                    { name: 'document',       groups: [ 'mode', 'document', 'doctools' ] },
                                    { name: 'others' },
                                    '/',
                                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                                    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                                    { name: 'styles' },
                                    { name: 'colors' },
                                ]
                            }}
                            events={{
                                "change": onChange
                            }}
                        />
                    </View>
                    <View style={{ width: "40%", margin: 20 }}>
                        <Button
                            icon={{
                                name: "save",
                                size: 20,
                                color: "white"
                            }}
                            title="ذخیره"
                            titleStyle={{ fontSize: 15 }}
                            onPress={save}
                            buttonStyle={{
                                borderRadius: 25,
                                width: 150,
                                backgroundColor: "green",
                                marginTop: 20
                            }}
                        />
                    </View>

                </Card>
            </Stack>
        </ScrollView>
    )
}