import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import { Button, Card } from "react-native-elements";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { PI } from "../Service/ApiCall";
import { html2json, json2html } from "html2json";
import { Label } from 'office-ui-fabric-react/lib/Label';
import CKEditor from "react-ckeditor-component";


export default class TwoWayBinding extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data
        };


        this.handleChange = this.handleChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
    }

    onEditorChange(evt) {
        this.setState({
            data: evt.editor.getData()
        });
    }

    handleChange(changeEvent) {
        this.setState({
            data: changeEvent.target.value
        });
    }

    async save() {
        var obj = {};
        obj.name = name;
        if (description) {
            obj.description = JSON.stringify(html2json(description));
        }
        try {
            if (props.route.params) {
                await PI.UpdatePI(props.route.params.id, obj);
            } else {
                await PI.CreatePI(obj);
            }
            alert("با موفقیت ذخیره شد.")
            setDescription("");
            setName("");
        } catch (error) {
            console.log(error);
        }
    }

    onChange(event) {
        const data = event.editor.getData();
        setDescription(data);
    }

    render() {

        let dec = this.props.data.description;

        console.log(this.state.name);


        if (typeof dec === 'undefined') {
            console.log("tttyyyyy")
            return (<></>)
        }

        return (
            <ScrollView>
                <Stack tokens={{ childrenGap: 15 }}>
                    <Card title="ایجاد شاخص">
                        <View style={{ width: "40%", margin: 20 }}>
                            <TextField
                                label="نام شاخص :"
                                required
                                value={this.props.data.name}
                                underlined
                            // onChange={(ev, name) => setName(name)}
                            />
                        </View>
                        <View style={{ width: "80%", margin: 20 }}>
                            <Label>توضیحات :</Label>
                            <CKEditor
                                content={dec}
                                scriptUrl="https://cdn.ckeditor.com/4.14.0/full-all/ckeditor.js"
                                config={{
                                    language: "fa",
                                    toolbarGroups: [
                                        { name: 'clipboard', groups: ['clipboard', 'undo'] },
                                        { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
                                        { name: 'links' },
                                        { name: 'insert' },
                                        { name: 'forms' },
                                        { name: 'tools' },
                                        { name: 'document', groups: ['mode', 'document', 'doctools'] },
                                        { name: 'others' },
                                        '/',
                                        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
                                        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
                                        { name: 'styles' },
                                        { name: 'colors' },
                                    ]
                                }}
                                events={{
                                    "change": this.onChange
                                }}
                            />
                        </View>
                        <View style={{ width: "40%", margin: 20 }}>
                            <Button
                                icon={{
                                    name: "save",
                                    size: 20,
                                    color: "white"
                                }}
                                title="ذخیره"
                                titleStyle={{ fontSize: 15 }}
                                onPress={this.save}
                                buttonStyle={{
                                    borderRadius: 25,
                                    width: 150,
                                    backgroundColor: "green",
                                    marginTop: 20
                                }}
                            />
                        </View>

                    </Card>
                </Stack>
            </ScrollView>
        )
    }
}
