import React, { useState } from 'react';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { DetailsList, DetailsListLayoutMode } from 'office-ui-fabric-react/lib/DetailsList';
import { MarqueeSelection } from 'office-ui-fabric-react/lib/MarqueeSelection';
import { View } from 'react-native';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { Card } from 'react-native-elements';
import { Questionnaire } from "../Service/ApiCall";
import { useFocusEffect } from '@react-navigation/native';
import { Link } from 'office-ui-fabric-react';



export default function QuestionnaireListScreen(props) {

    const _columns = [
        { key: 'column1', name: 'نام', fieldName: 'name', minWidth: 100, maxWidth: 200, isResizable: true, isPadded: true, data: 'string' },
        { key: 'column2', name: 'توضیحات', fieldName: 'description', minWidth: 100, maxWidth: 200, isResizable: true, data: 'string', isPadded: true },
        { key: 'column3', name: 'عملیات', fieldName: 'action', minWidth: 100, maxWidth: 200, isResizable: true, isPadded: true },

    ];



    const [state, setState] = useState({
        items: []
    })

    const _maxResult = 5000

    const [_allItems, setAllItems] = useState([]);

    const _getQuestionnaire = async () => {
        let mo = await Questionnaire.GetQuestionnaires(_maxResult);
        setAllItems(mo.data.items);
        setState({ items: mo.data.items });
    }

    useFocusEffect(
        React.useCallback(() => {
            _getQuestionnaire();
        }, [])
    )


    const _onFilter = (ev, text) => {
        var mo = text ? _allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1) : _allItems
        setState({
            items: mo
        });
    };
    const _onItemInvoked = (item) => {
        alert(`Item invoked: ${item.name}`);
    };

    const _goToEditQuestionnaire = (item) => {
        props.navigation.push("CreateQuestionnaireScreen", item);
    }

    const _removeQuestionnaire = async (item) => {
        console.log(item);
        if (confirm("آیا مطمئن هستید؟")) {
            try {
                await Questionnaire.DeleteQuestionnaire(item.id);
            } catch (error) {
                alert("نخست زیر مجموعه ها را حدف کنید");
            }
            _getQuestionnaire();
        }
    }

    // const { items, selectionDetails } = state;

    const _onRenderItemColumn = (item, index, column) => {
        if (column.fieldName === 'action') {
            return (
                <>
                    <Link onClick={() => _goToEditQuestionnaire(item)}>ویرایش</Link>
                    <Link
                        style={{ marginRight: 10, color: "red" }}
                        onClick={() => _removeQuestionnaire(item)}
                    >
                        حذف
               </Link>
                </>
            )
        }
        return item[column.fieldName];
    }


    return (
        <View >
            <Stack tokens={{ childrenGap: 15 }}>
                <Card title='لیست پرسشنامه' >

                    <TextField
                        required underlined
                        label="جستجو با عنوان:"
                        onChange={_onFilter}
                        styles={{ root: { width: 500, marginBottom: 20 } }}
                    />
                    <Fabric >
                        <View style={{
                            height: 500,

                            overflowY: 'auto'
                        }}>
                            <MarqueeSelection  >
                                <DetailsList
                                    onRenderItemColumn={_onRenderItemColumn}
                                    items={state.items}
                                    columns={_columns}
                                    setKey="set"
                                    compact={false}
                                    layoutMode={DetailsListLayoutMode.justified}
                                    selectionPreservedOnEmptyClick={true}
                                    ariaLabelForSelectionColumn="Toggle selection"
                                    ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                                    checkButtonAriaLabel="Row checkbox"
                                    onItemInvoked={_onItemInvoked}
                                />
                            </MarqueeSelection>
                        </View>
                    </Fabric>


                </Card>
            </Stack>
        </View>
    );
}








