import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Button, Card } from "react-native-elements";
import {
  PA,
  Position,
  PAOrganizationEntry,
  Department
} from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";

export default function PAOrganizationEntryScreen(props) {
  const dropdownStyles = {
    dropdown: { width: 300 }
  };

  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "name",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "نوع",
      fieldName: "organizatuinEntryTypeIndex",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column3",
      name: "عملیات",
      fieldName: "action",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [option, setOption] = useState([]);
  const [optionDepartment, setOptionDepartment] = useState([]);

  const [allItems, setAllItems] = useState([]);

  const [modalVisible, setModalVisible] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [dSelectedKeys, setDSelectedKeys] = useState([]);

  const _onDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    if (e.selected) {
      setSelectedKeys([...selectedKeys, newValue]);
    } else {
      let sk = [...selectedKeys];

      var index = sk.indexOf(newValue);
      if (index !== -1) sk.splice(index, 1);
      setSelectedKeys(sk);
    }
  };

  const _onDepartmentDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    if (e.selected) {
      setDSelectedKeys([...dSelectedKeys, newValue]);
    } else {
      let sk = [...dSelectedKeys];
      var index = sk.indexOf(newValue);
      if (index !== -1) sk.splice(index, 1);
      setDSelectedKeys(sk);
    }
  };

  const _makeOption = (data, orgEntry) => {
    let list = [];
    let po = [...data.items];

    po.forEach(element => {
      let index = orgEntry
        .map(e => {
          return e.id;
        })
        .indexOf(element.id);

      if (index === -1) {
        list.push({ key: element.id, text: element.name });
      } else {
        list.push({
          key: element.id,
          text: element.name,
          disabled: true
        });
      }
    });
    return list;
  };

  const _makeList = data => {
    let array = data.paOrganizationEntrys;
    let _item = [];
    for (let i = 0; i < data.paOrganizationEntrys.length; i++) {
      const organizationEntry = array[i].organizationEntry;
      organizationEntry.pAoId = array[i].id;
      _item.push(organizationEntry);
    }
    return _item;
  };

  const _makeDepartmentOption = (data, orgEntry) => {
    let list = [];
    let po = [...data];

    po.forEach(element => {
      let index = orgEntry
        .map(e => {
          return e.id;
        })
        .indexOf(element.organizationEntryId);

      if (index === -1) {
        list.push({
          key: element.organizationEntryId,
          text: element.path2Root
        });
      } else {
        list.push({
          key: element.organizationEntryId,
          text: element.path2Root,
          disabled: true
        });
      }
    });
    return list;
  };

  const _makePositionOption = (data, orgEntry) => {
    let list = [];
    let po = [...data];

    po.forEach(element => {
      let index = orgEntry
        .map(e => {
          return e.id;
        })
        .indexOf(element.organizationEntryId);

      if (index === -1) {
        list.push({
          key: element.organizationEntryId,
          text: element.path2Root
        });
      } else {
        list.push({
          key: element.organizationEntryId,
          text: element.path2Root,
          disabled: true
        });
      }
    });
    return list;
  };

  const _getPAOrganizationEntry = async () => {
    let mo = await PA.GetPA(props.route.params.id);
    let _item = _makeList(mo.data);
    setAllItems(_item);
    setState({ items: _item });
    let mo2 = await Position.GetPositionsPath();
    let mo3 = await Department.GetDepartmentPath();
    let _item2 = _makePositionOption(mo2.data, _item);
    let _item3 = _makeDepartmentOption(mo3.data, _item);
    setOption(_item2);
    setOptionDepartment(_item3);
  };

  useFocusEffect(
    React.useCallback(() => {
      _getPAOrganizationEntry();
    }, [])
  );

  const _onFilter = (ev, text) => {
    var mo = text
      ? allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1)
      : allItems;
    setState({
      items: mo
    });
  };

  const _onItemInvoked = item => {
    alert(`Item invoked: ${item.name}`);
  };

  const _goToSelectPACategory = item => {
    props.navigation.push("PAOrganizationEntryCategoryScreen", item);
  };

  const _removePaO = async item => {
    if (confirm("آیا مطمئن هستید؟")) {
      try {
        await PAOrganizationEntry.DeletePAOrganizationEntry(item.pAoId);
      } catch (error) {
        alert("نخست زیر مجموعه ها را حدف کنید");
      }
      _getPAOrganizationEntry();
    }
  };

  const _goToSelectPACategoryPI = item => {
    props.navigation.push("PAOrganizationEntryCategoryPIScreen", item);
  };

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link onClick={() => _goToSelectPACategory(item)}>
            مدیریت دسته بندی ها
          </Link>

          <Link
            style={{ marginRight: 10 }}
            onClick={() => _goToSelectPACategoryPI(item)}
          >
            مدیریت شاخص ها
          </Link>

          <Link
            style={{ marginRight: 10, color: "red" }}
            onClick={() => _removePaO(item)}
          >
            حذف
          </Link>
        </>
      );
    }
    if (column.fieldName === "organizatuinEntryTypeIndex") {
      if (item.organizatuinEntryTypeIndex == 1) {
        return <>دپارتمان</>;
      } else {
        return <>سمت</>;
      }
    }
    return item[column.fieldName];
  };

  const addPAOrganizationEntry = () => {
    let promiseArr = [];

    let sks = [...selectedKeys];

    let skd = [...dSelectedKeys];

    skd.forEach(element => {
      sks.push(element);
    });

    allItems.forEach(element => {
      var index = sks.indexOf(element.id);
      if (index !== -1) sks.splice(index, 1);
    });

    for (let i = 0; i < sks.length; i++) {
      var promise = new Promise(async (resolve, reject) => {
        var data = {
          organizationEntryId: sks[i],
          paId: props.route.params.id
        };
        try {
          let mo = await PAOrganizationEntry.CreatePAOrganizationEntry(data);
          resolve();
        } catch (error) {
          console.log(err);
          reject();
        }
      });

      promiseArr.push(promise);
    }

    Promise.all(promiseArr).then(values => {
      alert("با موفقیت ذخیره شد.");
      _getPAOrganizationEntry();
      setModalVisible(false);
    });
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisible}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "flex-end"
                }}
              >
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisible(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>
              <View style={styles.lineStyle} />
              <View>
                <Dropdown
                  placeholder="انتخاب سمت"
                  label="انتخاب سمت"
                  options={option}
                  selectedKeys={selectedKeys}
                  multiSelect={true}
                  onChange={_onDropdownChanged.bind(this)}
                />
              </View>

              <View>
                <Dropdown
                  placeholder="انتخاب دپارتمان"
                  label="انتخاب دپارتمان"
                  options={optionDepartment}
                  selectedKeys={dSelectedKeys}
                  multiSelect={true}
                  onChange={_onDepartmentDropdownChanged.bind(this)}
                />
              </View>

              <View
                style={{ alignItems: "flex-start", alignContent: "center" }}
              >
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={addPAOrganizationEntry}
                  buttonStyle={{
                    borderRadius: 25,
                    width: 150,
                    backgroundColor: "green",
                    marginTop: 20
                  }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>
        <Card title="لیست سمت های دوره ی ارزیابی">
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <TextField
                required
                underlined
                label="جستجو با عنوان:"
                onChange={_onFilter}
                styles={{ root: { width: 500, marginBottom: 20 } }}
              />
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <Button
                icon={{
                  name: "add",
                  size: 20,
                  color: "white"
                }}
                title="افزودن سمت و دپارتمان"
                titleStyle={{ fontSize: 15 }}
                onPress={() => {
                  setSelectedKeys([]);
                  setModalVisible(true);
                }}
                buttonStyle={{
                  borderRadius: 25,
                  width: 220,
                  backgroundColor: "#6a0dad"
                }}
              />
            </View>
          </View>
          <Fabric>
            <View
              style={{
                height: 500,

                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  selectionPreservedOnEmptyClick={true}
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
