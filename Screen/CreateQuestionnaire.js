import React, { useState, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import { Button, Card } from 'react-native-elements';
import styles from '../styles';
import Modal from 'modal-enhanced-react-native-web';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Questionnaire } from "../Service/ApiCall";
import { useFocusEffect } from '@react-navigation/native';
import { FontIcon } from 'office-ui-fabric-react/lib/Icon';





export default function CreateQuestionnaireScreen(props) {

  const [items, setItems] = useState([{
    id: null,
    content: null,
    weight: null,
    questionItems: [{
      id: null,
      name: "",
      value: "",
    }]
  }]);
  const [questionItems, setQuestionItems] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [questionnaireTitle, setQuestionnaireTitle] = useState("");
  const [questionnaireId, setQuestionnaireId] = useState(null);

  const [questionnaireDescription, setQuestionnaireDescription] = useState("");
  const [qIndex, setQIndex] = useState(null);
  const AddQuestion = () => {
    setItems([
      ...items,
      {
        content: null,
        weight: null,
        questionItems: [{
          name: "",
          value: "",
        }]
      }
    ]);
  }


  const _getQuestionnaire = async (id) => {
    let mo = await Questionnaire.GetQuestionnaire(id);
    setQuestionnaireTitle(mo.data.name);
    setQuestionnaireDescription(mo.data.description);
    setQuestionnaireId(id);
    setItems(mo.data.questions);
  }

  useFocusEffect(
    React.useCallback(() => {
      if (props.route.params) {

        _getQuestionnaire(props.route.params.id);
      }
    }, [])
  )

  const AddQuestionItem = () => {
    setQuestionItems([
      ...questionItems,
      {
        name: "",
        value: "",
      }
    ]);
  }

  const updateQuestionContent = (index, content) => {
    let it = [...items];
    it[index].content = content;
    setItems(it)
  }

  const updateQuestionWeight = (index, weight) => {
    let it = [...items];
    it[index].weight = weight;
    setItems(it)

  }

  const showQItemModal = (modalVisible, index) => {
    setQuestionItems(items[index].questionItems);
    setQIndex(index);
    setModalVisible(true);
  }

  const updateQuestionItemName = (index, name) => {
    let qi = [...questionItems];
    qi[index].name = name;
    setQuestionItems(qi);

  }

  const updateQuestionItemValue = (index, value) => {
    let qi = [...questionItems];
    qi[index].value = value;
    setQuestionItems(qi);

  }

  const AddQuestionItemToQuestion = () => {
    items[qIndex].questionItems = questionItems;
    setItems(items);
    setModalVisible(false)
    //setQuestionItems([]);
  }


  const SaveQestionnaire = async () => {

    let questionList = [];
    let model = {};
    for (let i = 0; i < items.length; i++) {
      let element = items[i];
      let questionObject = {};
      questionObject.id = element.id;
      questionObject.content = element.content;
      questionObject.weight = element.weight;
      questionObject.questionItems = element.questionItems;
      questionList.push(questionObject);
    }

    model.name = questionnaireTitle;
    model.description = questionnaireDescription
    model.questions = questionList;

    if (questionnaireId) {
      var mo = await Questionnaire.UpdateQuestionnaire(questionnaireId, model);
      if (mo.data.id > 0) {
        alert("باموفقیت ویرایش شد")
        ResetState()
      }
    } else {
      var mo = await Questionnaire.CreateQuestionnaire(model);
      if (mo.data.id > 0) {
        alert("باموفقیت ثبت شد")
        ResetState()
      }
    }
  }


  const ResetState = () => {
    setItems([{
      content: null,
      weight: null,
      questionItems: [{
        name: "",
        value: "",
      }]
    }]);
    setQuestionItems([]);
    setQuestionnaireTitle("");
    setQuestionnaireDescription("");
    setQuestionnaireId(null);
    setQIndex(null)
  }

  const removeItem = (index) => {
    let qi = [...questionItems];
    qi.splice(index, 1)
    setQuestionItems(qi);
  }

  const removeQuestion = (index) => {
    let it = [...items];
    it.splice(index,1),
    setItems(it);
  }

  return (
    <ScrollView>
      <Stack tokens={{ childrenGap: 15 }}>

        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisible}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-end", justifyContent: "space-between" }}>
                <Button
                  icon={{
                    name: "add",
                    size: 20,
                    color: "white"
                  }}
                  title="افزودن آیتم"
                  titleStyle={{ fontSize: 15 }}
                  onPress={AddQuestionItem}
                  buttonStyle={{ borderRadius: 25, width: 150, backgroundColor: "#6a0dad" }}
                />
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisible(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>

              <View style={styles.lineStyle} />

              <View >
                {questionItems.map((item, index) => (
                  <View key={index} style={{ marginTop: 15, flexDirection: "row" }}>
                    <View style={{ flex: 8 }}>
                      <TextField label="عنوان:" required underlined value={item.name} onChange={(ev, name) => updateQuestionItemName(index, name)} />
                    </View>
                    <View style={{ flex: 4, marginHorizontal: 10 }}>
                      <TextField label="امتیاز:" required underlined type="number" inputMode="decimal" value={item.value} onChange={(ev, value) => updateQuestionItemValue(index, value)} />
                    </View>
                    <View style={{ flex: 1, marginHorizontal: 10 }}>
                      <FontIcon iconName="Cancel" style={{ color: "red", padding: 10, cursor: "pointer" }} onClick={() => removeItem(index)} />

                    </View>
                  </View>
                ))}
              </View>

              <View style={{ alignItems: "flex-start", alignContent: "center" }}>
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={AddQuestionItemToQuestion}
                  buttonStyle={{ borderRadius: 25, width: 150, backgroundColor: "green", marginTop: 20 }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>

        <Card title='ایجاد پرسشنامه' >
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 2 }}>
              <TextField label="نام پرسشنامه:" required value={questionnaireTitle} underlined onChange={(ev, title) => setQuestionnaireTitle(title)} />
            </View>
            <View style={{ flex: 4, marginHorizontal: 10 }}>
              <TextField label="توضیحات :" underlined value={questionnaireDescription} onChange={(ev, description) => setQuestionnaireDescription(description)} />
            </View>
            <View style={{ flex: 4, alignItems: "flex-end", justifyContent: "flex-end" }}>
              <Button
                icon={{
                  name: "add",
                  size: 20,
                  color: "white"
                }}
                title="افزودن سوال"
                onPress={AddQuestion}
                buttonStyle={{ borderRadius: 25, backgroundColor: "#6a0dad" }}
              />
            </View>
          </View>

          <View style={{ flex: 1, marginTop: 15 }}>
            {items.map((item, index) => (
              <View key={index} style={{ marginTop: 15, flexDirection: "row" }}>
                <View style={{ flex: 8 }}>
                  <TextField label="متن سوال:" required underlined value={item.content} onChange={(ev, content) => updateQuestionContent(index, content)} />
                </View>
                <View style={{ flex: 2, marginHorizontal: 10 }}>
                  <TextField label="وزن:" required underlined type="number" value={item.weight} inputMode="decimal" onChange={(ev, weight) => updateQuestionWeight(index, weight)} />
                </View>
                <View style={{ flex: 2, alignItems: "flex-end", justifyContent: "flex-end", flexDirection: "row" }}>
                  <Button
                    icon={{
                      name: "add",
                      size: 20,
                      color: "white"
                    }}
                    onPress={() => {
                      showQItemModal(true, index)
                    }}
                    buttonStyle={{ borderRadius: 25, width: 50, backgroundColor: "#6a0dad" }}
                  />

                  <FontIcon iconName="Cancel" style={{ color: "red", padding: 10, cursor: "pointer" }} onClick={() => removeQuestion(index)} />


                </View>



              </View>

            ))}
          </View>
          <View style={{ alignItems: "flex-start", alignContent: "center" }}>
            <Button
              icon={{
                name: "save",
                size: 20,
                color: "white"
              }}
              title="ذخیره"
              titleStyle={{ fontSize: 15 }}
              onPress={SaveQestionnaire}
              buttonStyle={{ borderRadius: 25, width: 150, backgroundColor: "green", marginTop: 20 }}
            />
          </View>
        </Card>
      </Stack>

    </ScrollView>
  );
}

