import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Button, Card } from "react-native-elements";
import {
  PA,
  Position,
  PAOrganizationEntry,
  PAOrganizationEntryCategory
} from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";

export default function PAOrganizationEntryCategoryScreen(props) {
  const dropdownStyles = {
    dropdown: { width: 300 }
  };

  const _option = [
    {
      id: 1,
      name: "Funcional",
      OperationEvaluationCategoryIndex: 0
    },
    {
      id: 2,
      name: "Corporate",
      OperationEvaluationCategoryIndex: 1
    },
    {
      id: 3,
      name: "Individual",
      OperationEvaluationCategoryIndex: 2
    },
    {
      id: 4,
      name: "Team",
      OperationEvaluationCategoryIndex: 3
    }
  ];

  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "name",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "وزن",
      fieldName: "weight",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column3",
      name: "عملیات",
      fieldName: "action",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [option, setOption] = useState([{}]);
  const [allItems, setAllItems] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState(null);
  const [weight, setWeight] = useState(1);
  const [id, setId] = useState(null);
  const [editMode, setEditMode] = useState(false);

  const _onDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setSelectedKeys(newValue);
    // if (event.selected) {
    //   setSelectedKeys([...selectedKeys, newValue]);
    // } else {
    //   let sk = [...selectedKeys];

    //   var index = sk.indexOf(newValue);
    //   if (index !== -1) sk.splice(index, 1);
    //   setSelectedKeys(sk);
    // }
  };
  const _makeOption = categories => {
    let list = [];
    let po = [..._option];

    po.forEach(element => {
      let index = categories
        .map(e => {
          return e.id;
        })
        .indexOf(element.id);

      if (index === -1) {
        list.push({ key: element.id, text: element.name });
      } else {
        list.push({
          key: element.id,
          text: element.name,
          disabled: true
        });
      }
    });
    return list;
  };
  const _makeList = data => {
    let array = data.paOrganizationEntryCategories;
    let _item = [];
    for (let i = 0; i < array.length; i++) {
      const operationEvaluationCategory = array[i].operationEvaluationCategory;
      operationEvaluationCategory.paOrganizationEntryCategoriesId = array[i].id;
      operationEvaluationCategory.weight = array[i].weight;
      _item.push(operationEvaluationCategory);
    }
    return _item;
  };
  const _getPAOrganizationEntryCategory = async () => {
    let mo = await PAOrganizationEntry.GetPAOrganizationEntry(
      props.route.params.pAoId
    );
    let _item = _makeList(mo.data);
    setAllItems(_item);
    setState({ items: _item });
    let _item2 = _makeOption(_item);
    setOption(_item2);
  };
  useFocusEffect(
    React.useCallback(() => {
      _getPAOrganizationEntryCategory();
    }, [])
  );
  const _onFilter = (ev, text) => {
    var mo = text
      ? allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1)
      : allItems;
    setState({
      items: mo
    });
  };
  const _onItemInvoked = item => {
    alert(`Item invoked: ${item.name}`);
  };
  const _removePaCategory = async item => {
    console.log(item)
    if (confirm("آیا مطمئن هستید؟")) {
      try {
        await PAOrganizationEntryCategory.DeletePAOrganizationEntryCategory(
          item.paOrganizationEntryCategoriesId
        );
      } catch (error) {
        alert("نخست زیر مجموعه ها را حدف کنید");
      }

      _getPAOrganizationEntryCategory();
    }
  };
  const _edit = item => {
    setWeight(item.weight);
    setSelectedKeys(item.id);
    setId(item.paOrganizationEntryCategoriesId);
    setEditMode(true);
    setModalVisible(true);
  };
  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link onClick={() => _edit(item)}>ویرایش</Link>
          <Link
            style={{ color: "red", marginRight: 10 }}
            onClick={() => _removePaCategory(item)}
          >
            حذف
          </Link>
        </>
      );
    }
    return item[column.fieldName];
  };
  const addPAOrganizationEntryCategory = () => {
    let promiseArr = [];

    var promise = new Promise(async (resolve, reject) => {
      var data = {
        operationEvaluationCategoryId: selectedKeys,
        paOrganizationEntryId: props.route.params.pAoId,
        weight: weight
      };
      try {
        if (editMode) {
          let mo = await PAOrganizationEntryCategory.UpdatePAOrganizationEntryCategory(
            id,
            data
          );
          resolve();
        } else {
          let mo = await PAOrganizationEntryCategory.CreatePAOrganizationEntryCategory(
            data
          );
          resolve();
        }
      } catch (error) {
        console.log(error);
        reject();
      }
    });

    promiseArr.push(promise);

    // for (let i = 0; i < sks.length; i++) {

    //   promiseArr.push(promise);
    // }

    Promise.all(promiseArr).then(values => {
      alert("با موفقیت ذخیره شد.");
      _getPAOrganizationEntryCategory();
      setModalVisible(false);
    });
  };
  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisible}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "flex-end"
                }}
              >
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisible(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>
              <View style={styles.lineStyle} />
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Dropdown
                    placeholder="انتخاب دسته بندی"
                    label="انتخاب دسته بندی"
                    options={option}
                    selectedKey={selectedKeys}
                    onChange={_onDropdownChanged.bind(this)}
                  />
                </View>
              </View>

              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <TextField
                    underlined
                    value={weight}
                    label="وزن"
                    type="number"
                    inputMode="decimal"
                    onChange={(ev, weight) => setWeight(weight)}
                    styles={{ root: { marginBottom: 20 } }}
                  />
                </View>
              </View>

              <View
                style={{ alignItems: "flex-start", alignContent: "center" }}
              >
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={addPAOrganizationEntryCategory}
                  buttonStyle={{
                    borderRadius: 25,
                    width: 150,
                    backgroundColor: "green",
                    marginTop: 20
                  }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>
        <Card title="لیست دسته بندی های سمت در دوره ی ارزیابی">
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <TextField
                required
                underlined
                label="جستجو با عنوان:"
                onChange={_onFilter}
                styles={{ root: { width: 500, marginBottom: 20 } }}
              />
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <Button
                icon={{
                  name: "add",
                  size: 20,
                  color: "white"
                }}
                title="افزودن دسته بندی"
                titleStyle={{ fontSize: 15 }}
                onPress={() => {
                  setSelectedKeys(null);
                  setEditMode(false);
                  setModalVisible(true);
                }}
                buttonStyle={{
                  borderRadius: 25,
                  width: 180,
                  backgroundColor: "#6a0dad"
                }}
              />
            </View>
          </View>
          <Fabric>
            <View
              style={{
                height: 500,
                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
