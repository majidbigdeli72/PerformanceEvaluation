import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Card, Button } from "react-native-elements";
import { PA, PAResultPI } from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import moment from "moment-jalaali";
import DatePicker from "../Component/Calender/index";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";

export default function EvaluationListScreen(props) {
  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "name",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "تاریخ شروع",
      fieldName: "startDate",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column3",
      name: "تاریخ پایان",
      fieldName: "endDate",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      data: "string",
      isPadded: true
    },
    {
      key: "column4",
      name: "عملیات",
      fieldName: "action",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [modalVisible, setModalVisible] = useState(false);

  const [_allItems, setAllItems] = useState([]);

  const [startDate, setStartDate] = useState(null);
  const [pA, setPA] = useState(0);

  //setStartDate(moment(mo.data.startDate, "jYYYY-jMM-jDDTHH:mm:ss").format());

  const [startDateMoment, setStartDateMoment] = useState(null);

  const _getPA = async () => {
    try {
      let mo = await PA.GetPAs();

      setAllItems(mo.data.items);
      setState({ items: mo.data.items });
    } catch (error) {}
  };

  useFocusEffect(
    React.useCallback(() => {
      _getPA();
    }, [])
  );

  const _onFilter = (ev, text) => {
    var mo = text
      ? _allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1)
      : _allItems;
    setState({
      items: mo
    });
  };
  const _onItemInvoked = item => {
    alert(`Item invoked: ${item.name}`);
  };

  const _goToEditPA = item => {
    props.navigation.push("CreateEvaluationScreen", item);
  };

  const _goToPAOrganizationEntry = item => {
    props.navigation.push("PAOrganizationEntryScreen", item);
  };

  const _removePA = async item => {
    if (confirm("آیا مطمئن هستید؟")) {
      try {
        await PA.DeletePa(item.id);
      } catch (error) {
        alert("نخست زیر مجموعه ها را حدف کنید");
      }
      _getPA();
    }
  };

  const _managePA = async item => {
    setPA(item.id);
    
    if (item.lastExecution) {
      setStartDateMoment(
        moment(item.lastExecution, "jYYYY-jMM-jDDTHH:mm:ss").add(1, "days")
      );
    }else{
      setStartDateMoment(
        moment(item.startDate, "jYYYY-jMM-jDDTHH:mm:ss")
      ); 
    }
    setModalVisible(true);
  };

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link onClick={() => _goToEditPA(item)}>ویرایش</Link>
          <Link
            style={{ marginRight: 10 }}
            onClick={() => _goToPAOrganizationEntry(item)}
          >
            مدیریت سمت و دپارتمان
          </Link>
          <Link style={{ marginRight: 10 }} onClick={() => _managePA(item)}>
            تغییر زمان اجرا
          </Link>
          <Link
            style={{ marginRight: 10, color: "red" }}
            onClick={() => _removePA(item)}
          >
            حذف
          </Link>
        </>
      );
    }
    if (column.fieldName === "startDate") {
      return moment(item.startDate).format("YYYY-MM-DD");
    }
    if (column.fieldName === "endDate") {
      return moment(item.endDate).format("YYYY-MM-DD");
    }
    return item[column.fieldName];
  };

  const SaveMPA = async () => {
    let model = {
      pAId: pA,
      relatedDate: startDate
    };
    try {
      var mo = await PAResultPI.DeletePAResultPi(model);
      alert("باموفقیت ویرایش شد");
      setModalVisible(false);
    } catch (error) {
      alert(error);
    }
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisible}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "flex-end"
                }}
              >
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisible(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>
              <View style={styles.lineStyle} />
              <View style={{ width: "60%" }}>
                <DatePicker
                  title="تاریخ شروع ارزیابی"
                  required={true}
                  isGregorian={false}
                  timePicker={false}
                  inputReadOnly={true}
                  value={startDateMoment}
                  onChange={value => {
                    setStartDate(value.format("YYYY-MM-DD"));
                  }}
                />
              </View>

              <View style={{ width: "40%", margin: 20 }}>
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={SaveMPA}
                  buttonStyle={{
                    borderRadius: 25,
                    width: 150,
                    backgroundColor: "green",
                    marginTop: 20
                  }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>

        <Card title="لیست دوره ی ارزیابی">
          <TextField
            required
            underlined
            label="جستجو با عنوان:"
            onChange={_onFilter}
            styles={{ root: { width: 500, marginBottom: 20 } }}
          />
          <Fabric>
            <View
              style={{
                height: 500,

                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
