import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Button, Card } from "react-native-elements";
import { Checkbox } from "office-ui-fabric-react/lib/Checkbox";
import {
  Questionnaire,
  PI,
  PAOrganizationEntryCategoryPI,
  PAOrganizationEntry
} from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";

export default function PAOrganizationEntryCategoryPIScreen(props) {
  const _columns = [
    {
      key: "column1",
      name: "نام",
      fieldName: "performanceIndicatorName",
      minWidth: 120,
      maxWidth: 210,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "پرسشنامه",
      fieldName: "questionnaireName",
      minWidth: 70,
      maxWidth: 120,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column3",
      name: "دسته بندی",
      fieldName: "operationEvaluationCategoryName",
      minWidth: 30,
      maxWidth: 60,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column4",
      name: "کمترین میزان",
      fieldName: "min",
      minWidth: 30,
      maxWidth: 75,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column5",
      name: " بیشترین میزان",
      fieldName: "max",
      minWidth: 30,
      maxWidth: 75,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column6",
      name: "دوره ی ارزیابی",
      fieldName: "evaluationPeriodIndex",
      minWidth: 30,
      maxWidth: 75,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column7",
      name: "نوع ارزیابی",
      fieldName: "evaluationTypeIndex",
      minWidth: 30,
      maxWidth: 75,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column8",
      name: "نوع محاسبه",
      fieldName: "calculationTypeIndex",
      minWidth: 30,
      maxWidth: 75,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column9",
      name: "ارزیابی گروهی",
      fieldName: "isGroupingEvaluation",
      minWidth: 25,
      maxWidth: 70,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column10",
      name: "عملیات",
      fieldName: "action",
      minWidth: 120,
      maxWidth: 150,
      isResizable: true,
      isPadded: true
    }
  ];
  const [state, setState] = useState({
    items: []
  });
  const [editMode, setEditMode] = useState(false);
  const [option, setOption] = useState([]);
  const [etOption, setEtOption] = useState([
    {
      key: 0,
      text: "پرسشنامه"
    },
    {
      key: 1,
      text: "دستی"
    }
  ]);
  const [qOption, setQOption] = useState([]);
  const [catOption, setCatOption] = useState([]);
  const [pOption, setPOption] = useState([
    {
      key: 0,
      text: "هفتگی"
    },
    {
      key: 1,
      text: "ماهیانه"
    },
    {
      key: 2,
      text: "فصلی"
    },
    {
      key: 3,
      text: "سالیانه"
    }
  ]);
  const [cOption, setCOption] = useState([
    {
      key: 0,
      text: "افزایشی"
    },
    {
      key: 1,
      text: "کاهشی"
    }
  ]);
  const [allItems, setAllItems] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState(null);
  const [etselectedKeys, setEtSelectedKeys] = useState(null);
  const [qSelectedKeys, setQSelectedKeys] = useState(null);
  const [catSelectedKeys, setCatSelectedKeys] = useState(null);
  const [pSelectedKeys, setPSelectedKeys] = useState(null);
  const [cSelectedKeys, setCSelectedKeys] = useState(null);
  const [qState, setQState] = useState(false);
  const [isGroupingEvaluation, setIsGroupingEvaluation] = useState(false);
  const [min, setMin] = useState(null);
  const [max, setMax] = useState(null);
  const [weight, setWeight] = useState(null);
  const [
    paOrganizationEntryCategoryId,
    setPaOrganizationEntryCategoryId
  ] = useState(null);
  const _onDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setSelectedKeys(newValue);
  };
  const _onEtDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setEtSelectedKeys(newValue);

    if (newValue == 1) {
      setQSelectedKeys(null);
      setQState(false);
    } else {
      setQState(true);
    }
  };
  const _onQDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setQSelectedKeys(newValue);
  };
  const _onPDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setPSelectedKeys(newValue);
  };
  const _onCDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setCSelectedKeys(newValue);
  };
  const _onCatDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setCatSelectedKeys(newValue);
  };
  const _makeOption = data => {
    let list = [];
    data.forEach(element => {
      list.push({ key: element.id, text: element.name });
    });
    return list;
  };
  const _makeList = data => {
    let array = data.paOrganizationEntryCategories;
    let _paOrganizationEntryCategories = [];
    let _list = [];

    array.forEach(element => {
      element.paOrganizationEntryCategoryPIs.forEach(element2 => {
        element2.operationEvaluationCategoryName =
          element.operationEvaluationCategory.name;
        _paOrganizationEntryCategories.push(element2);
      });

      if (element.operationEvaluationCategory) {
        _list.push({
          key: element.id,
          text: element.operationEvaluationCategory.name
        });
      }
    });

    setCatOption(_list);

    let _item = [];
    for (let i = 0; i < _paOrganizationEntryCategories.length; i++) {
      const pi = _paOrganizationEntryCategories[i];
      pi.performanceIndicatorName =
        _paOrganizationEntryCategories[i].performanceIndicator.name;

      if (_paOrganizationEntryCategories[i].questionnaire) {
        pi.questionnaireName =
          _paOrganizationEntryCategories[i].questionnaire.name;
      }
      _item.push(pi);
    }
    return _item;
  };
  const _getPAOrganizationEntryCategoryPI = async () => {
    let mo = await PAOrganizationEntry.GetPAOrganizationEntry(
      props.route.params.pAoId
    );
    let _item = _makeList(mo.data);

    setAllItems(_item);
    setState({ items: _item });
  };
  const _getPi = async () => {
    let mo = await PI.GetPIs();
    let _item = _makeOption(mo.data.items);
    setOption(_item);
  };
  const _getQuestionnaire = async () => {
    let mo = await Questionnaire.GetQuestionnaires();
    let _item = _makeOption(mo.data.items);
    setQOption(_item);
  };
  useFocusEffect(
    React.useCallback(() => {
      _getPAOrganizationEntryCategoryPI();
      _getPi();
      _getQuestionnaire();
    }, [])
  );
  const _onFilter = (ev, text) => {
    var mo = text
      ? allItems.filter(
          i => i.performanceIndicatorName.toLowerCase().indexOf(text) > -1
        )
      : allItems;
    setState({
      items: mo
    });
  };
  const _onMin = (ev, text) => {
    setMin(text);
  };
  const _onMax = (ev, text) => {
    setMax(text);
  };
  const _onWeight = (ev, text) => {
    setWeight(text);
  };
  const _onItemInvoked = item => {
    alert(`Item invoked: ${item.name}`);
  };
  const openAssessor = item => {
    props.navigation.push("PAOrganizationEntryCategoryPIAssessorScreen", item);
  };
  const _removePI = async item => {
    if (confirm("آیا مطمئن هستید؟")) {
      try {
        await PAOrganizationEntryCategoryPI.DeletePAOrganizationEntryCategoryPI(
          item.id
        );
      } catch (error) {
        alert("نخست زیر مجموعه ها را حدف کنید");
S      }

      _getPAOrganizationEntryCategoryPI();
    }
  };
  const _edit = item => {
    console.log(item);
    setSelectedKeys(item.performanceIndicatorId);
    setEtSelectedKeys(item.evaluationTypeIndex);
    setPSelectedKeys(item.evaluationPeriodIndex);
    setCSelectedKeys(item.calculationTypeIndex);
    setMin(item.min);
    setMax(item.max);
    setWeight(item.weight);
    setCatSelectedKeys(item.paOrganizationEntryCategoryId);
    setIsGroupingEvaluation(item.isGroupingEvaluation);
    setPaOrganizationEntryCategoryId(item.id);
    if (item.evaluationTypeIndex == 0) {
      setQState(true);
      setQSelectedKeys(item.questionnaireId);
    }

    setEditMode(true);
    setModalVisible(true);
  };
  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link onClick={() => openAssessor(item)}>انتخاب ارزیابی کننده</Link>
          <Link style={{ marginRight: 10 }} onClick={() => _edit(item)}>
            ویرایش
          </Link>
          <Link
            style={{ marginRight: 10, color: "red" }}
            onClick={() => _removePI(item)}
          >
            حذف
          </Link>
        </>
      );
    }

    if (column.fieldName === "isGroupingEvaluation") {
      if (item.isGroupingEvaluation == true) {
        return <>دارد</>;
      } else {
        return <>ندارد</>;
      }
    }

    if (column.fieldName === "evaluationPeriodIndex") {
      if (item.evaluationPeriodIndex == 0) {
        return <>هفتگی</>;
      } else if (item.evaluationPeriodIndex == 1) {
        return <>ماهیانه</>;
      } else if (item.evaluationPeriodIndex == 2) {
        return <>فصلی</>;
      } else {
        return <>سالیانه</>;
      }
    }

    if (column.fieldName === "calculationTypeIndex") {
      if (item.calculationTypeIndex == 0) {
        return <>افزایشی</>;
      } else {
        return <>کاهشی</>;
      }
    }

    if (column.fieldName === "evaluationTypeIndex") {
      if (item.evaluationTypeIndex == 0) {
        return <>پرسشنامه</>;
      }
      return <>دستی</>;
    }

    return item[column.fieldName];
  };
  const addPAOrganizationEntryCategoryPI = () => {
    let data = {};
    //Todo:// get from DropDown
    data.paOrganizationEntryCategoryId = catSelectedKeys;
    data.performanceIndicatorId = selectedKeys;
    if (qSelectedKeys) {
      data.questionnaireId = qSelectedKeys;
    }

    data.weight = weight;
    data.evaluationPeriodIndex = pSelectedKeys;
    data.evaluationTypeIndex = etselectedKeys;
    data.calculationTypeIndex = cSelectedKeys;
    data.min = min;
    data.max = max;

    data.isGroupingEvaluation = isGroupingEvaluation;

    var promise = new Promise(async (resolve, reject) => {
      try {
        if (editMode) {
          let mo = await PAOrganizationEntryCategoryPI.UpdatePAOrganizationEntryCategoryPI(
            paOrganizationEntryCategoryId,
            data
          );

          if (mo.data.id > 0) {
            resolve();
          } else {
            reject();
          }
        } else {
          let mo = await PAOrganizationEntryCategoryPI.CreatePAOrganizationEntryCategoryPI(
            data
          );

          if (mo.data.id > 0) {
            resolve();
          } else {
            reject();
          }
        }
      } catch (error) {
        console.log(error);
        reject();
      }
    });

    promise.then(values => {
      alert("با موفقیت ذخیره شد.");
      setModalVisible(false);
      _getPAOrganizationEntryCategoryPI();
      _restState();
    });
  };
  const _restState = () => {
    setSelectedKeys(null);
    setEtSelectedKeys(null);
    setQSelectedKeys(null);
    setPSelectedKeys(null);
    setCSelectedKeys(null);
    setMin(null);
    setMax(null);
    setWeight(null);
    setQState(false);
  };
  const _questionnaire = () => {
    if (qState) {
      return (
        <Dropdown
          placeholder="انتخاب پرسشنامه"
          label="پرسشنامه"
          options={qOption}
          selectedKey={qSelectedKeys}
          onChange={_onQDropdownChanged.bind(this)}
        />
      );
    } else {
      return <></>;
    }
  };
  const _onGroupChange = (event, value) => {
    setIsGroupingEvaluation(value);
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisible}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "flex-end"
                }}
              >
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisible(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>
              <View style={styles.lineStyle} />

              <View style={{ marginBottom: 20, flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Dropdown
                    placeholder="انتخاب دسته بندی"
                    label="انتخاب دسته بندی"
                    options={catOption}
                    selectedKey={catSelectedKeys}
                    onChange={_onCatDropdownChanged.bind(this)}
                  />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 4 }}>
                  <Dropdown
                    placeholder="انتخاب ارزیابی"
                    label="انتخاب ارزیابی"
                    options={option}
                    selectedKey={selectedKeys}
                    onChange={_onDropdownChanged.bind(this)}
                  />
                </View>
                <View style={{ flex: 1 }}></View>

                <View style={{ flex: 4 }}>
                  <Dropdown
                    placeholder="توع ارزیابی"
                    label="نوع ارزیابی"
                    options={etOption}
                    selectedKey={etselectedKeys}
                    onChange={_onEtDropdownChanged.bind(this)}
                  />
                </View>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={{ marginBottom: 20, flex: 4 }}>
                  <Dropdown
                    placeholder="دوره ی ارزیابی"
                    label="دوره ی ارزیابی"
                    options={pOption}
                    selectedKey={pSelectedKeys}
                    onChange={_onPDropdownChanged.bind(this)}
                  />
                </View>

                <View style={{ flex: 1 }}></View>

                <View style={{ marginBottom: 20, flex: 4 }}>
                  <Dropdown
                    placeholder="نحوه محاسبه"
                    label="نحوه محاسبه"
                    options={cOption}
                    selectedKey={cSelectedKeys}
                    onChange={_onCDropdownChanged.bind(this)}
                  />
                </View>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 4 }}>
                  <TextField
                    underlined
                    value={min}
                    label="حداقل"
                    onChange={_onMin}
                    styles={{ root: { marginBottom: 20 } }}
                  />
                </View>
                <View style={{ flex: 1 }}></View>
                <View style={{ flex: 4 }}>
                  <TextField
                    underlined
                    value={max}
                    label="حداکثر"
                    onChange={_onMax}
                    styles={{ root: { marginBottom: 20 } }}
                  />
                </View>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 4 }}>
                  <TextField
                    underlined
                    label="وزن"
                    value={weight}
                    onChange={_onWeight}
                    styles={{ root: { marginBottom: 20 } }}
                  />
                </View>
                <View style={{ flex: 1 }}></View>
                <View style={{ flex: 4 }}>
                  <_questionnaire />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Checkbox
                    boxSide="end"
                    label="ارزیابی گروهی"
                    checked={isGroupingEvaluation}
                    onChange={_onGroupChange}
                  />
                </View>
              </View>
              <View
                style={{ alignItems: "flex-start", alignContent: "center" }}
              >
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={addPAOrganizationEntryCategoryPI}
                  buttonStyle={{
                    borderRadius: 25,
                    width: 150,
                    backgroundColor: "green",
                    marginTop: 20
                  }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>
        <Card title="لیست ارزیابی های دوره دسته بندی">
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <TextField
                required
                underlined
                label="جستجو با عنوان:"
                onChange={_onFilter}
                styles={{ root: { width: 500, marginBottom: 20 } }}
              />
            </View>
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <Button
                icon={{
                  name: "add",
                  size: 20,
                  color: "white"
                }}
                title="افزودن ارزیابی"
                titleStyle={{ fontSize: 15 }}
                onPress={() => {
                  setSelectedKeys([]);
                  setEditMode(false);
                  setModalVisible(true);
                }}
                buttonStyle={{
                  borderRadius: 25,
                  width: 150,
                  backgroundColor: "#6a0dad"
                }}
              />
            </View>
          </View>
          <Fabric>
            <View
              style={{
                height: 500,

                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
