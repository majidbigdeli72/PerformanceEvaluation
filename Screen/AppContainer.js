import React, { useState, useEffect, useRef } from "react";
import { NavigationContainer, useLinking } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "./Login";
import ChangePasswordScreen from "./ChangePassword";
import Dashboard from "./Dashboard";
import refs from "../Service/Navigate";
import "../app.css";
import AutoSubmitForm from "./AutoSubmitForm";

const Stack = createStackNavigator();
const PERSISTENCE_KEY = "NAVIGATION_STATE";

export default function AppContainer() {
  const [initialState, setInitialState] = useState();
  const [isReady, setIsReady] = useState(false);
  const [token, setToken] = useState("");
  const [submit, setSubmit] = useState(false);
  const [returnUrl, setReturnUrl] = useState("/LoginViaRequest.aspx");

  useEffect(() => {
    if (window.location.search) {
      localStorage.removeItem("token");
      localStorage.setItem(PERSISTENCE_KEY,'{"stale":false,"type":"stack","key":"stack-ysqXcdpi4","index":1,"routeNames":["LoginScreen","Dashboard","ChangePasswordScreen"],"routes":[{"key":"Dashboard-yGxbmSLMk","name":"Dashboard","params":{"Stack":{}},"state":{"stale":false,"type":"stack","key":"stack-8O81n6jP1","index":1,"routeNames":["CreateQuestionnaireScreen","QuestionnaireListScreen","CreateEvaluationScreen","EvaluationListScreen","PAOrganizationEntryScreen","PAOrganizationEntryCategoryScreen","PAOrganizationEntryCategoryPIScreen","PAOrganizationEntryCategoryPIAssessorScreen","PAResultPIAssessorScreen"],"routes":[{"key":"PAResultPIAssessorScreen-fLTCM-aBe","name":"PAResultPIAssessorScreen"},{"key":"QuestionnaireListScreen-uktptEnr","name":"QuestionnaireListScreen"}]}},{"key":"LoginScreen-fzd6DVXQR","name":"LoginScreen"}]}')
    }
    const restoreState = async () => {
      try {
        const savedStateString = await localStorage.getItem(PERSISTENCE_KEY);
        const state = JSON.parse(savedStateString);
        setInitialState(state);
      } finally {
        setIsReady(true);
      }
    };

    if (!isReady) {
      restoreState();
    }
  }, [isReady]);

  if (!isReady) {
    return null;
  }

  return (
    <>
      <NavigationContainer
        ref={n => (refs.navigation = n)}
        initialState={initialState}
        onStateChange={state => {
          debugger;
          localStorage.setItem(PERSISTENCE_KEY, JSON.stringify(state));
        }}
      >
        {
          <Stack.Navigator initialRouteName={"Dashboard"} headerMode="none">
            <Stack.Screen
              options={{ title: "ورود" }}
              name="LoginScreen"
              key="LoginScreen"
              component={LoginScreen}
            />
            <Stack.Screen
              options={{ title: "داشبورد", headerShown: false }}
              name="Dashboard"
              key="Dashboard"
              component={Dashboard}
              initialParams={{ Stack: Stack }}
            />
            <Stack.Screen
              options={{ title: "داشبورد", headerShown: false }}
              name="ChangePasswordScreen"
              key="ChangePasswordScreen"
              component={ChangePasswordScreen}
              initialParams={{ Stack: Stack }}
            />
          </Stack.Navigator>
        }
      </NavigationContainer>

      <AutoSubmitForm
        actionUrl={returnUrl}
        params={{ token: token }}
        autoSubmit={submit}
      />
    </>
  );
}
