import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Button, Card } from "react-native-elements";
import {
  Employee,
  Position,
  PAOrganizationEntryCategory,
  PAOrganizationEntryCategoryPI,
  PAOrganizationEntryCategoryPIAssessor
} from "../Service/ApiCall";
import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";

export default function PAOrganizationEntryCategoryPIAssessorScreen(props) {
  const _columns = [
    {
      key: "column1",
      name: "نوع",
      fieldName: "assessorTypeIndex",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column2",
      name: "نام",
      fieldName: "name",
      minWidth: 50,
      maxWidth: 100,
      isResizable: true,
      isPadded: true,
      data: "string"
    },
    {
      key: "column3",
      name: "عملیات",
      fieldName: "action",
      minWidth: 100,
      maxWidth: 200,
      isResizable: true,
      isPadded: true
    }
  ];

  const [state, setState] = useState({
    items: []
  });

  const [option, setOption] = useState([{}]);

  const [allItems, setAllItems] = useState([]);

  const [modalVisibleAssessor, setModalVisibleAssessor] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState(null);
  const [weight, setWeight] = useState(0);

  const [positionOption, setPositionOption] = useState([]);
  const [employeeOption, setEmployeeOption] = useState([]);
  const [pState, setPState] = useState(false);
  const [eState, setEState] = useState(false);

  const [positionSelectedKeys, setPositionSelectedKeys] = useState(null);
  const [employeeSelectedKeys, setEmployeeSelectedKeys] = useState(null);
  const [assessorSelectedKeys, setAssessorSelectedKeys] = useState(null);
  const [assessorOption, setAssessorOption] = useState([
    {
      key: 0,
      text: "مدیر"
    },
    {
      key: 1,
      text: "همکار"
    },
    {
      key: 2,
      text: "سمت"
    },
    {
      key: 3,
      text: "کارمند"
    }
  ]);

  const _makeOption = data => {
    let list = [];
    data.forEach(element => {
      list.push({ key: element.id, text: element.name });
    });
    return list;
  };
  const _makeList = data => {
    let array = data.paOrganizationEntryCategoryPIAssessors;
    let _item = [];

    array.forEach(element => {
      let obj = {};
      obj.assessorTypeIndex = element.assessorTypeIndex;

      if (element.organizationEntry) {
        obj.name = element.organizationEntry.name;
      }
      obj.id = element.id;
      _item.push(obj);
    });
    return _item;
  };

  const _getPAOrganizationEntryCategoryPI = async () => {
    let mo = await PAOrganizationEntryCategoryPI.GetPAOrganizationEntryCategoryPI(
      props.route.params.id
    );
    let _item = _makeList(mo.data);
    setAllItems(_item);
    setState({ items: _item });
    let _item2 = _makeOption(_item);
    setOption(_item2);
  };

  const _getPosition = async () => {
    let mo = await Position.GetPositionsPath();
    let _option = [];
    mo.data.forEach(element => {
      _option.push({
        key: element.organizationEntryId,
        text: element.path2Root
      });
    });
    setPositionOption(_option);
  };
  const _getEmployee = async () => {
    let mo = await Employee.GetEmployees();
    let _option = [];
    mo.data.items.forEach(element => {
      _option.push({
        key: element.employee.id,
        text: element.employee.fullName
      });
    });
    setEmployeeOption(_option);
  };

  useFocusEffect(
    React.useCallback(() => {
      _getPAOrganizationEntryCategoryPI();
      _getPosition();
      _getEmployee();
    }, [])
  );

  const _onFilter = (ev, text) => {
    var mo = text
      ? allItems.filter(i => i.name.toLowerCase().indexOf(text) > -1)
      : allItems;
    setState({
      items: mo
    });
  };

  const _onItemInvoked = item => {
    alert(`Item invoked: ${item.name}`);
  };

  const _removeAssessor = async item => {
    if (confirm("آیا مطمئن هستید؟")) {
      await PAOrganizationEntryCategoryPIAssessor.DeletePositionCategoryPIAssessor(
        item.id
      );
      _getPAOrganizationEntryCategoryPI();
    }
  };

  const _onRenderItemColumn = (item, index, column) => {
    if (column.fieldName === "action") {
      return (
        <>
          <Link style={{ color: "red" }} onClick={() => _removeAssessor(item)}>
            حذف
          </Link>
        </>
      );
    }
    if (column.fieldName === "assessorTypeIndex") {
      if (item.assessorTypeIndex == 0) {
        return <>مدیر</>;
      }

      if (item.assessorTypeIndex == 1) {
        return <>همکار</>;
      }

      if (item.assessorTypeIndex == 2) {
        return <>سمت</>;
      }

      if (item.assessorTypeIndex == 3) {
        return <>کارمند</>;
      }
    }
    return item[column.fieldName];
  };

  const _onAssessorDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setAssessorSelectedKeys(newValue);
    if (newValue === 2) {
      if (!pState) {
        setPState(true);
      }
    } else {
      setPState(false);
      setPositionSelectedKeys([]);
    }
    if (newValue === 3) {
      if (!eState) {
        setEState(true);
      }
    } else {
      setEState(false);
      setEmployeeSelectedKeys([]);
    }
  };

  const _onEmployeeDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setEmployeeSelectedKeys(newValue);
  };

  const _onPositionDropdownChanged = (h, e,i) => {
    var newValue = e.key;
    setPositionSelectedKeys(newValue);
  };

  const addAssessorForPI = async () => {
    let data = {};

    data.paOrganizationEntryCategoryPIId = props.route.params.id;
    data.assessorTypeIndex = assessorSelectedKeys;
    if (assessorSelectedKeys == 2) {
      if (positionSelectedKeys == null) {
        alert("سمتی انتخاب نشده است");
        return;
      }
      data.customAssessorId = positionSelectedKeys;
    }

    if (assessorSelectedKeys == 3) {
      if (employeeSelectedKeys == null) {
        alert("کارمندی انتخاب نشده است");
        return;
      }
      data.customAssessorId = employeeSelectedKeys;
    }

    try {
      
      let mo = await PAOrganizationEntryCategoryPIAssessor.CreatePositionCategoryPIAssessor(
        data
      );
      
      alert("با موفقیت ذخیره شد.");
      _getPAOrganizationEntryCategoryPI();
      setModalVisibleAssessor(false);
      setPState(false);
      setEState(false);
      setPositionSelectedKeys(null);
      setEmployeeSelectedKeys(null);
      setAssessorSelectedKeys(null);
    } catch (error) {
      console.log(error);
    }
  };

  const _position = () => {
    if (pState) {
      return (
        <Dropdown
          label="سمت"
          placeholder="انتخاب سمت"
          options={positionOption}
          selectedKey={positionSelectedKeys}
          onChange={_onPositionDropdownChanged.bind(this)}
        />
      );
    } else {
      return <></>;
    }
  };
  const _employee = () => {
    if (eState) {
      return (
        <Dropdown
          label="کارمند"
          placeholder="انتخاب کارمند"
          options={employeeOption}
          selectedKey={employeeSelectedKeys}
          onChange={_onEmployeeDropdownChanged.bind(this)}
        />
      );
    } else {
      return <></>;
    }
  };

  return (
    <View>
      <Stack tokens={{ childrenGap: 15 }}>
        <View>
          <Modal
            backdropOpacity={0.5}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropTransitionInTiming={1000}
            backdropTransitionOutTiming={1000}
            isVisible={modalVisibleAssessor}
            style={styles.topModal}
          >
            <ScrollView style={styles.modalContent}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "flex-end"
                }}
              >
                <Button
                  icon={{
                    name: "close",
                    size: 20,
                    color: "white"
                  }}
                  onPress={() => setModalVisibleAssessor(false)}
                  buttonStyle={{ borderRadius: 25, backgroundColor: "red" }}
                />
              </View>
              <View style={styles.lineStyle} />
              <View style={{ marginBottom: 20, flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Dropdown
                    label="ارزیابی کننده"
                    placeholder="انتخاب ارزیابی کننده"
                    options={assessorOption}
                    selectedKey={assessorSelectedKeys}
                    onChange={_onAssessorDropdownChanged.bind(this)}
                  />
                </View>
              </View>
              <View style={{ marginBottom: 20, flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <_position />
                  <_employee />
                </View>
              </View>
              <View
                style={{ alignItems: "flex-start", alignContent: "center" }}
              >
                <Button
                  icon={{
                    name: "save",
                    size: 20,
                    color: "white"
                  }}
                  title="ذخیره"
                  titleStyle={{ fontSize: 15 }}
                  onPress={addAssessorForPI}
                  buttonStyle={{
                    borderRadius: 25,
                    width: 150,
                    backgroundColor: "green",
                    marginTop: 20
                  }}
                />
              </View>
            </ScrollView>
          </Modal>
        </View>
        <Card title="ارزیابی کننده ها">
          <View style={{ flex: 1, flexDirection: "row" }}>
            {/* <View style={{ flex: 1 }}>
              <TextField
                required
                underlined
                label="جستجو با عنوان:"
                onChange={_onFilter}
                styles={{ root: { width: 500, marginBottom: 20 } }}
              />
            </View> */}
            <View style={{ flex: 1, alignItems: "flex-end" }}>
              <Button
                icon={{
                  name: "add",
                  size: 20,
                  color: "white"
                }}
                title="افزودن ارزیابی کننده"
                titleStyle={{ fontSize: 15 }}
                onPress={() => {
                  setModalVisibleAssessor(true);
                }}
                buttonStyle={{
                  borderRadius: 25,
                  width: 200,
                  backgroundColor: "#6a0dad"
                }}
              />
            </View>
          </View>
          <Fabric>
            <View
              style={{
                height: 500,
                overflowY: "auto"
              }}
            >
              <MarqueeSelection>
                <DetailsList
                  onRenderItemColumn={_onRenderItemColumn}
                  items={state.items}
                  columns={_columns}
                  setKey="set"
                  compact={false}
                  layoutMode={DetailsListLayoutMode.justified}
                  selectionPreservedOnEmptyClick={true}
                  ariaLabelForSelectionColumn="Toggle selection"
                  ariaLabelForSelectAllCheckbox="Toggle selection for all items"
                  checkButtonAriaLabel="Row checkbox"
                  onItemInvoked={_onItemInvoked}
                />
              </MarqueeSelection>
            </View>
          </Fabric>
        </Card>
      </Stack>
    </View>
  );
}
