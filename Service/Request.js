import axios from "axios";
import refs from "./Navigate.js";
import { CommonActions } from "@react-navigation/native";

// window.location.origin + "/api"
const apiConfig = {
  baseUrl: window.location.origin + "/api" 
};

export const Request = {
  async post(url, data = {}, config) {
    try {
      const response = await axios.post(apiConfig.baseUrl + url, data, config);
      return response;
    } catch (error) {
      if (error.response.status === 401) {
        localStorage.removeItem("token");
        refs.navigation.dispatch(
          CommonActions.navigate({
            name: "LoginScreen"
          })
        );
        throw error;
      }

      if (error.response.status === 403) {
        if (error.response.data) {
          alert(error.response.data.error.message);
        } else {
          alert("امکان دسترسی به این صفحه را ندارید.");
        }

        throw error;
      }

      throw error;
    }
  },
  async get(url, config, params) {
    try {
      const response = await axios.get(apiConfig.baseUrl + url, config, {
        params
      });
      return response;
    } catch (error) {
      if (error.response.status === 401 || error.response.status === 500) {
        localStorage.removeItem("token");
        refs.navigation.dispatch(
          CommonActions.navigate({
            name: "LoginScreen"
          })
        );
        throw error;
      }

      if (error.response.status === 403) {
        if (error.response.data) {
          alert(error.response.data.error.message);
        } else {
          alert("امکان دسترسی به این صفحه را ندارید.");
        }

        throw error;
      }

      throw error;
    }
  },
  async put(url, data = {}, config) {
    try {
      const response = await axios.put(apiConfig.baseUrl + url, data, config);
      return response;
    } catch (error) {
      if (error.response.status === 401) {
        localStorage.removeItem("token");
        refs.navigation.dispatch(
          CommonActions.navigate({
            name: "LoginScreen"
          })
        );
        throw error;
      }

      if (error.response.status === 403) {
        if (error.response.data) {
          alert(error.response.data.error.message);
        } else {
          alert("امکان دسترسی به این صفحه را ندارید.");
        }

        throw error;
      }

      throw error;
    }
  },
  async delete(url, config) {
    try {
      const response = await axios.delete(apiConfig.baseUrl + url, config);
      return response;
    } catch (error) {
      if (error.response.status === 401) {
        localStorage.removeItem("token");
        refs.navigation.dispatch(
          CommonActions.navigate({
            name: "LoginScreen"
          })
        );
        throw error;
      }

      if (error.response.status === 403) {
        if (error.response.data) {
          alert(error.response.data.error.message);
        } else {
          alert("امکان دسترسی به این صفحه را ندارید.");
        }

        throw error;
      }

      throw error;
    }
  }
};
