import { Request } from "./Request";
const qs = require('querystring')


function getCookie(name) {
  // var pattern = RegExp(name + "=.[^;]*")
  // var matched = document.cookie.match(pattern)
  // if(matched){
  //     var cookie = matched[0].split('=')
  //     return cookie[1]
  // }
  // return false
  var token = localStorage.getItem("token");
  return token;
}

export const Questionnaire = {
  async CreateQuestionnaire(data) {
    return await Request.post("/api/app/questionnaire", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetQuestionnaires(maxResult = 10000) {
    return await Request.get(
      `/api/app/questionnaire?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetQuestionnaire(id) {
    return await Request.get(`/api/app/questionnaire/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdateQuestionnaire(id, model) {
    return await Request.put(`/api/app/questionnaire/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  },
  async DeleteQuestionnaire(id) {
    return await Request.delete(`/api/app/questionnaire/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }

};

export const QuestionResult = {
  async GetQuestionResult(questionnaireResultId) {
    return await Request.get(`/api/app/questionResult/questionResult/${questionnaireResultId}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

}

export const Position = {
  async CreatePosition(data) {
    return await Request.post("/api/app/position", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPositions(maxResult = 10000, sort = "name asc") {
    return await Request.get(
      `/api/app/position?MaxResultCount=${maxResult}&Sorting=${sort}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPosition(id) {
    return await Request.get(`/api/app/position/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePosition(id, model) {
    return await Request.put(`/api/app/position/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  },
  async GetPositionsPath() {
    return await Request.get(`/api/app/position/positionPath`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  }
};

export const Department = {
  async CreateDepartment(data) {
    return await Request.post("/api/app/department", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetDepartments(maxResult = 10000, sort = "name asc") {
    return await Request.get(
      `/api/app/department?MaxResultCount=${maxResult}&Sorting=${sort}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetDepartment(id) {
    return await Request.get(`/api/app/department/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdateDepartment(id, model) {
    return await Request.put(`/api/app/department/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetDepartmentPath() {
    return await Request.get(`/api/app/department/departmentPath`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  }
};

export const PA = {
  async CreatePA(data) {
    return await Request.post("/api/app/pA", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPAs(maxResult = 10000) {
    return await Request.get(`/api/app/pA?MaxResultCount=${maxResult}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async GetPA(id) {
    return await Request.get(`/api/app/pA/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePA(id, model) {
    return await Request.put(`/api/app/pA/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async DeletePa(id) {
    return await Request.delete(`/api/app/pA/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PALog = {
  async GetPALogs(maxResult = 10000) {
    return await Request.get(`/api/app/pALog/pALogs`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  }
};

export const PAOrganizationEntry = {
  async CreatePAOrganizationEntry(data) {
    return await Request.post("/api/app/pAOrganizationEntry", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPAOrganizationEntrys(maxResult = 10000) {
    return await Request.get(
      `/api/app/pAOrganizationEntry?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPAOrganizationEntry(id) {
    return await Request.get(`/api/app/pAOrganizationEntry/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePAOrganizationEntry(id, model) {
    return await Request.put(`/api/app/pAOrganizationEntry/${id}`, model, {
      headers: {
        //Header Defination
        accept: "application/json",
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async DeletePAOrganizationEntry(id) {
    return await Request.delete(`/api/app/pAOrganizationEntry/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PAOrganizationEntryCategory = {
  async CreatePAOrganizationEntryCategory(data) {
    return await Request.post("/api/app/pAOrganizationEntryCategory", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPAOrganizationEntryCategories(maxResult = 10000) {
    return await Request.get(
      `/api/app/pAOrganizationEntryCategory?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPAOrganizationEntryCategory(id) {
    return await Request.get(`/api/app/pAOrganizationEntryCategory/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePAOrganizationEntryCategory(id, model) {
    return await Request.put(
      `/api/app/pAOrganizationEntryCategory/${id}`,
      model,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async DeletePAOrganizationEntryCategory(id) {
    return await Request.delete(`/api/app/pAOrganizationEntryCategory/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PI = {
  async CreatePI(data) {
    return await Request.post("/api/app/performanceIndicator", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPIs(maxResult = 10000) {
    return await Request.get(
      `/api/app/performanceIndicator?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPI(id) {
    return await Request.get(`/api/app/performanceIndicator/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePI(id, model) {
    return await Request.put(`/api/app/performanceIndicator/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PAOrganizationEntryCategoryPI = {
  async CreatePAOrganizationEntryCategoryPI(data) {
    return await Request.post("/api/app/pAOrganizationEntryCategoryPI", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetPAOrganizationEntryCategoriesPI(maxResult = 10000) {
    return await Request.get(
      `/api/app/pAOrganizationEntryCategoryPI?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPAOrganizationEntryCategoryPI(id) {
    return await Request.get(`/api/app/pAOrganizationEntryCategoryPI/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdatePAOrganizationEntryCategoryPI(id, model) {
    return await Request.put(
      `/api/app/pAOrganizationEntryCategoryPI/${id}`,
      model,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async DeletePAOrganizationEntryCategoryPI(id, model) {
    return await Request.delete(
      `/api/app/pAOrganizationEntryCategoryPI/${id}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  }
};



export const User = {
  async HasRole(data) {
    return await Request.post("/api/app/user/hasRole", qs.stringify(data), {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  }
};

export const Employee = {
  async CreateEmployee(data) {
    return await Request.post("/api/app/employee", data, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async GetEmployees(maxResult = 10000, sort = "name asc") {
    return await Request.get(
      `/api/app/employee?MaxResultCount=${maxResult}&Sorting=${sort}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetEmployee(id) {
    return await Request.get(`/api/app/employee/${id}`, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json"
      }
    });
  },

  async UpdateEmployee(id, model) {
    return await Request.put(`/api/app/employee/${id}`, model, {
      headers: {
        //Header Defination
        Authorization: `Bearer ${getCookie("token")}`,
        accept: "application/json",
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PAOrganizationEntryCategoryPIAssessor = {
  async CreatePositionCategoryPIAssessor(data) {
    return await Request.post(
      "/api/app/pAOrganizationEntryCategoryPIAssessor",
      data,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },

  async GetPositionCategoryPIAssessors(maxResult = 10000) {
    return await Request.get(
      `/api/app/pAOrganizationEntryCategoryPIAssessor?MaxResultCount=${maxResult}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async GetPositionCategoryPIAssessor(id) {
    return await Request.get(
      `/api/app/pAOrganizationEntryCategoryPIAssessor/${id}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json"
        }
      }
    );
  },

  async UpdatePositionCategoryPIAssessor(id, model) {
    return await Request.put(
      `/api/app/pAOrganizationEntryCategoryPIAssessor/${id}`,
      model,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },

  async DeletePositionCategoryPIAssessor(id) {
    return await Request.delete(
      `/api/app/pAOrganizationEntryCategoryPIAssessor/${id}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  }
};

export const Auth = {
  async Connect(data) {
    return await Request.post("/connect/token", data, {
      headers: {
        //Header Defination
        "Content-Type": "application/json-patch+json"
      }
    });
  },
  async Login(data) {
    return await Request.post("/api/app/account/login", data, {
      headers: {
        //Header Defination
        "Content-Type": "application/json-patch+json"
      }
    });
  },

  async ChangePassword(data) {
    return await Request.post("/api/app/account/changePassword", data, {
      headers: {
        //Header Defination
        "Content-Type": "application/json-patch+json"
      }
    });
  }
};

export const PAResultPIAssessor = {
  async GetPAResultPIAssessor() {
    return await Request.get(
      "/api/app/pAResultPIAssessor/curentUserPAResultPI",
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async GetPAResultPIAssessorById(assessorId) {
    return await Request.get(
      `/api/app/pAResultPIAssessor/assessorPAResultPI/${assessorId}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async savePAResultPIAssessor(data) {
    return await Request.post(
      "/api/app/pAResultPIAssessor/savePAResultPIAssessor",
      data,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async ByPassResultPIAssessor(paResultPIAssessorId) {
    return await Request.post(
      `/api/app/pAResultPIAssessor/byPassPaResultPIAssessor/${paResultPIAssessorId}`,
      {},
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async ChangeAssessor(data) {
    return await Request.post(
      `/api/app/pAResultPIAssessor/changeAssessor?paResultPIAssessorId=${data.paResultPIAssessorId}&assessedById=${data.assessedById}`,
      {},
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
  async ChangeEmployee(data) {
    return await Request.post(
      `/api/app/pAResultPIAssessor/changePAResultPIEmployee?paResultPIId=${data.paResultPIId}&employeeId=${data.employeeId}`,
      {},
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  },
};

export const QuestionnaireResult = {
  async CreateQuestionnaireResult(data) {
    return await Request.post(
      "/api/app/questionnaireResult/questionnaireResult",
      data,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  }
};

export const PAResultPI = {
  async DeletePAResultPi(data) {
    return await Request.delete(
      `/api/app/pAResultPI?pAId=${data.pAId}&relatedDate=${data.relatedDate}`,
      {
        headers: {
          //Header Defination
          Authorization: `Bearer ${getCookie("token")}`,
          accept: "application/json",
          "Content-Type": "application/json-patch+json"
        }
      }
    );
  }
};
