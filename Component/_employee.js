import React, { useState } from "react";
import { TextField } from "office-ui-fabric-react/lib/TextField";
import {
  DetailsList,
  DetailsListLayoutMode
} from "office-ui-fabric-react/lib/DetailsList";
import { MarqueeSelection } from "office-ui-fabric-react/lib/MarqueeSelection";
import { View, ScrollView } from "react-native";
import { Stack } from "office-ui-fabric-react/lib/Stack";
import { Fabric } from "office-ui-fabric-react/lib/Fabric";
import { Button, Card } from "react-native-elements";

import { useFocusEffect } from "@react-navigation/native";
import { Link } from "office-ui-fabric-react";
import styles from "../styles";
import Modal from "modal-enhanced-react-native-web";
import { Dropdown } from "office-ui-fabric-react/lib/Dropdown";

class _employee extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    
    return this.props.eState !== nextProps.eState;
  }

  render() {
    if (this.props.eState) {
      return (
        <Dropdown
          label="کارمند"
          placeholder="انتخاب کارمند"
          multiSelect
          // options={employeeOption}
          //  selectedKeys={employeeSelectedKeys}
          //  onChange={_onEmployeeDropdownChanged.bind(this)}
        />
      );
    } else {
      return <></>;
    }
  }
}

