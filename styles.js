import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 22,
       // justifyContent: 'center',
        //alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    topModal: {
        justifyContent: 'flex-start',
        marginTop: 30,
        marginHorizontal:550,
        
    },
    topModal2: {
        justifyContent: 'flex-start',
        marginTop: 30,
        
    },
    scrollableModal: {
        height: 300,
    },
    scrollableModalContent1: {
        height: 200,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollableModalContent2: {
        height: 200,
        backgroundColor: 'lightgreen',
        alignItems: 'center',
        justifyContent: 'center',
    },

    lineStyle:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
        marginTop:15
   }

});
